#!/usr/bin/env ruby
# encoding: utf-8
# Sample Test:Unit based test case using the selenium-client API
#
require "selenium-webdriver"
require 'spreadsheet'
gem "test-unit"
require "test/unit"
require "../classes/scripts"
require "./class"

class Cad <Test::Unit::TestCase
	def setup
		@driver = Selenium::WebDriver.for :firefox
    	@base_url = "http://store-qa.ibacloud.com.br"
    	@accept_next_alert = true
    	@driver.manage.timeouts.implicit_wait = 60
    	@verification_errors = []
	end

  	def teardown
    	@driver.quit
    	assert_equal [], @verification_errors
  	end
  
  	def element_present?(how, what)
    	@driver.find_element(how, what)
    true
 	rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  	end
  
  	def alert_present?()
    	@driver.switch_to.alert
    true
  	rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  	end
  
  	def verify(&blk)
    yield
  	rescue Test::Unit::AssertionFailedError => ex
    	@verification_errors << ex
  	end
  
  	def close_alert_and_get_its_text(how, what)
    	alert = @driver.switch_to().alert()
    	alert_text = alert.text
    if (@accept_next_alert) then
      	alert.accept()
    else
      	alert.dismiss()
    end
    	alert_text
  	ensure
    	@accept_next_alert = true
  	end

  	def acessa
  		acessabanco = AcessaBanco.new
  	end

  	def test_teste
  		acessa
  		@driver.get (@base_url)
  		@driver.find_element(:css, "html.js body.welcome-controller div.container div#main-content div#getting-started-modal.modal div.modal-body")
    	@driver.find_element(:css, "button.close").click
    	@driver.find_element(:id, "header-sign-up-link").click
    	@driver.find_element(:id, "sign_up_email").send_keys acessa.email
  		
  	end
end