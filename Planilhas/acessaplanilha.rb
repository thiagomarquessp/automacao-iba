#!/usr/bin/env ruby
# encoding: utf-8
# Sample Test:Unit based test case using the selenium-client API
#
require "selenium-webdriver"
require 'spreadsheet'
gem "test-unit"
require "test/unit"
require "../classes/scripts"


class AcessaBanco < Spreadsheet::Excel::Row

attr_reader :email

  	def initialize
  		book = Spreadsheet.open('/home/cataclisma/Desktop/NovasPraticas/Planilhas/teste.xls')
		#p book.worksheet 'Sheet1'
		sheet1 = book.worksheet('planteste') # can use an index or worksheet name
		sheet1.each do |row|
  		return if row[2].nil? # if first cell empty
	 		#puts row.join('|') # looks like it calls "to_s" on each cell's Value
			@email = row[1]	
		end
  		return @email
  	end
end