# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#
require "json"
require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require '../classes/scripts'
require '../classes/massausuario'
### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects
### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.
### Mais um para se basear em melhores práticas: http://ithaca.arpinum.org/2010/07/29/ruby-dynamic-includes.html
### Para executar algum método específico, utilizar o seguinte comando: ruby -I"lib:test" ~/Desktop/Automacaoo/loja/webstore.rb -n test_cadastro_cliente
###  sales@froglogic.com.
class Loja < Test::Unit::TestCase

  def connection_refused
    display = ENV['BUILD_NUMBER'] || "99"
    @headless = Headless.new(:display => display)
    @headless.start
  end

  def setup
    @driver = Selenium::WebDriver.for :firefox
    @base_url = "http://store-qa.ibacloud.com.br/"
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 60
    @verification_errors = []
  end
 
  def teardown
    @driver.quit
    assert_equal [], @verification_errors
  end
  
  def element_present?(how, what)
    @driver.find_element(how, what)
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end
  
  def alert_present?()
    @driver.switch_to.alert
    true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  end
  
  def verify(&blk)
    yield
  rescue Test::Unit::AssertionFailedError => ex
    @verification_errors << ex
  end
  
  def close_alert_and_get_its_text(how, what)
    alert = @driver.switch_to().alert()
    alert_text = alert.text
    if (@accept_next_alert) then
      alert.accept()
    else
      alert.dismiss()
    end
    alert_text
  ensure
    @accept_next_alert = true
  end

  def  scripts_initialize
    scripts = ScriptsJs.new
  end

  def  massausuario
      @massa ||= MassadeDados.new
  end

  def usuario
    usuario_existente = Usuarios.new
  end

  def pgtos
    @pagamento ||= Pagamentos.new(massausuario)
  end

  def validahistorico
    @history ||= Historicos.new
  end

### Valida Nome de Revista ###

  def test_validanomerevista
    @driver.get ("https://admin-qa.ibacloud.com.br")
    @driver.find_element(:id, "administrator_email").send_keys "admin@iba.com"
    @driver.find_element(:id, "administrator_password").send_keys "password"
    @driver.find_element(:name, "commit" ).click
  end

### Método de Login. Será utilizado sempre ###
  def login
    massausuario.fake_generator(@driver)
    @driver.get(@base_url)
    @driver.find_element(:id, "header-login-link").click
    @driver.find_element(:id, "login_email").send_keys massausuario.email
    @driver.find_element(:id, "login_password").send_keys "inicial1234"
    @driver.find_element(:xpath, "/html/body/div[3]/div[2]/div/div[2]/div/form/fieldset[3]/button").click
  end
### Fim do Método de Login. Utilizar em todos os métodos que irão realizar o Login ###

### Método de Logout. ###
  def logout
    @driver.find_element(:id, "user-name-link").click
    @driver.find_element(:id, "user-name-link").click
  end
### Fim do Método de Logout ### 

### \o/ Aqui inicia os testes \o/ ###
### Metodo de Cadastro de Cliente.
### Foi utilizado 
  def cadastro_cliente
    massausuario.fake_generator(@driver)
    @driver.get(@base_url)
    @driver.find_element(:css, "html.js body.welcome-controller div.container div#main-content div#getting-started-modal.modal div.modal-body")
    @driver.find_element(:css, "button.close").click
    @driver.find_element(:id, "header-sign-up-link").click
    @driver.find_element(:id, "sign_up_email").send_keys massausuario.email
    @driver.find_element(:id, "sign_up_email_confirmation").send_keys massausuario.email
    @driver.find_element(:id, "sign_up_name").send_keys massausuario.nome
    @driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
    @driver.find_element(:id, "sign_up_password_confirmation").send_keys massausuario.senha_padrao
    @driver.find_element(:id, "sign_up_terms_and_conditions").click
    @driver.find_element(:css, "html.js body.users-controller div.container div#main-content div#login-sign-up.page div.new div.sign-up-holder form#sign-up-form.sign-up-form div.login fieldset.action-buttons button.btn").click
    sleep 10
    verify {assert(@driver.find_element(:id, "coupon-modal").text.include?("Obrigado pelo seu cadastro!") == true)}
    @driver.find_element(:css, "html.js body.users-controller div.container div#main-content div#login-sign-up.page div#coupon-modal.modal div.modal-footer button.btn").click
  end
### Fim do Método de Cadastro de Cliente ###

### Método de Primeira Compra de Revistas ###
  def primeira_compra_revistas
    test_cadastro_cliente
    @driver.get(@base_url)
    @driver.find_element(:xpath, "/html/body/header/nav/a/span").click
    @driver.find_element(:id, "show_more_link").click
    @driver.find_element(:id, "show_more_link").click
    scripts_initialize.select_random_product_for(@driver, "comprar")
    #scripts_initialize.select_random_product_for(@driver, "resgatar-gratis")
    pgtos.realizar_primeiro_pgto(@driver)
    validahistorico.primeiracompra(@driver)
    verify {assert(@driver.find_element(:xpath, "/html/body/div[3]/div[2]/section/div/ul/li[3]/ul/li/span[2]").text.include?("#{validahistorico.nm_rev_comprada}"))}
    verify {assert(@driver.find_element(:xpath, "/html/body/div[3]/div[2]/section/div/ul/li[3]/ul/li/div").text.include?("#{validahistorico.nm_prod_comprado}"))}
    sleep 10
  end
### Fim do Método de Primeira Compra de Revistas ###

### Método de Novas Compras de Revistas. Utilizar o método de usuário existente para não ter que criar diversos usuários. ####
  def nova_compra_revistas
    usuario.existente(@driver)
    @driver.find_element(:xpath, "/html/body/header/nav/a/span").click
    @driver.find_element(:id, "show_more_link").click
    @driver.find_element(:id, "show_more_link").click
    scripts_initialize.select_random_product_for(@driver, "resgatar-gratis")
    pgtos.realizar_novo_pagamento(@driver)
    #validahistorico.novacompra_gratuita(@driver)
    #verify {assert(@driver.find_element(:xpath, "/html/body/div[3]/div[2]/section/div/ul/li[3]/ul/li/span[2]").text.include?("#{@hist_ncg}"))}
    sleep 05
    #verify {assert element_present?(:css, "html.js body.order-controller div.container div#main-content div#order-success.order-confirmation p.successful-purchase")}
### Fim do método de Novas Compras ###
  end

### Método de Primeira Compra de Ebooks ###
  def primeira_compra_ebooks
    cadastro_cliente
    @driver.find_element(:xpath, "/html/body/header/nav/a[2]/span").click
    @driver.find_element(:id, "show_more_link").click
    @driver.find_element(:id, "show_more_link").click
    scripts_initialize.select_random_product_for(@driver, "comprar")
    pgtos.realizar_primeiro_pgto(@driver)
    validahistorico.hist_pce(@driver)
    verify {assert(@driver.find_element(:xpath, "/html/body/div[3]/div[2]/section/div/ul/li[3]/ul/li/span[2]").text.include?("#{@historicoeb}"))}
    sleep 10
  end
### Fim do Método de Primeira Compra de Ebooks ###

### Método de Nova Compra de Ebooks ###
  def nova_compra_ebooks
    usuario.existente(@driver)
    @driver.find_element(:xpath, "/html/body/header/nav/a[2]/span").click
    @driver.find_element(:id, "show_more_link").click
    @driver.find_element(:id, "show_more_link").click
    scripts_initialize.select_random_product_for_ebooks(@driver)
    pgtos.realizar_novo_pagamento(@driver)
    verify { assert(@driver.find_element(:css, "html.js body.order-controller div.container div#main-content div#order-success.order-confirmation p.successful-purchase").text.include?("O seu pedido foi finalizado com sucesso."),"Page contains the text NAME")}
  end
### Fim do Método de Nova Compra de Ebooks ###

### Método de Primeira Compra de Jornais ###
  def primeira_compra_jornais
    cadastro_cliente
    @driver.find_element(:xpath, "/html/body/header/nav/a[3]/span").click
    @driver.find_element(:id, "show_more_link").click
    @driver.find_element(:id, "show_more_link").click
    scripts_initialize.select_random_product_for(@driver, "comprar")
    pgtos.realizar_primeiro_pgto(@driver)
    sleep 10
  end
end
