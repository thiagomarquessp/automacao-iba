# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects
### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.
### Mais um para se basear em melhores práticas: http://ithaca.arpinum.org/2010/07/29/ruby-dynamic-includes.html
### Para executar algum método específico, utilizar o seguinte comando: ruby -I"lib:test" ~/Desktop/Automacaoo/loja/webstore.rb -n test_cadastro_cliente
###  sales@froglogic.com.

require "json"
require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require '../classes/scripts'
require '../classes/massausuario'
require '../classes/pagamentos'

class CompraRevista <Test::Unit::TestCase

def setup
  @driver = Selenium::WebDriver.for :firefox
  @base_url = "http://store-qa.ibacloud.com.br/"
  @accept_next_alert = true
  @driver.manage.timeouts.implicit_wait = 60
  @verification_errors = []
end

def teardown
  @driver.quit
  assert_equal [], @verification_errors
end
  
def element_present?(how, what)
  @driver.find_element(how, what)
  true
  rescue Selenium::WebDriver::Error::NoSuchElementError
  false
end
  
def alert_present?()
  @driver.switch_to.alert
  true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
  false
end
  
def verify(&blk)
  yield
  rescue Test::Unit::AssertionFailedError => ex
  @verification_errors << ex
end
  
def close_alert_and_get_its_text(how, what)
  alert = @driver.switch_to().alert()
  alert_text = alert.text
  if (@accept_next_alert) then
  alert.accept()
  else
  alert.dismiss()
  end
  alert_text
  ensure
  @accept_next_alert = true
end

def  scripts_initialize
  scripts = ScriptsJs.new
end


def test_busca_principal_nome_revista
  puts "Fluxo principal de busca na store por nome de revista específica."
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "criteria").send_keys "Veja"
  @driver.find_element(:id, "submit-search").click
  sleep 10
  
end



end
