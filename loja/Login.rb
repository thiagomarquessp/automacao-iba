# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.

require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require '../classes/scripts'
require '../classes/massausuario'

class Login < Test::Unit::TestCase

### Configurações e chamadas de outros métodos e classes de outros arquivos 
  def setup
    @driver = Selenium::WebDriver.for :firefox
    @base_url = "https://store-qa.ibacloud.com.br/"
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 60
    @verification_errors = []
  end

  def teardown
    @driver.quit
    assert_equal [], @verification_errors
  end
  
  def element_present?(how, what)
    @driver.find_element(how, what)
    true
    rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end
  
  def alert_present?()
    @driver.switch_to.alert
    true
    rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  end
  
  def verify(&blk)
    yield
    rescue Test::Unit::AssertionFailedError => ex
    @verification_errors << ex
  end
  
  def close_alert_and_get_its_text(how, what)
    alert = @driver.switch_to().alert()
    alert_text = alert.text
      if (@accept_next_alert) then
          alert.accept()
      else
          alert.dismiss()
      end
          alert_text
      ensure
          @accept_next_alert = true
  end

  def  scripts_initialize
    scripts = ScriptsJs.new
  end

  def  massausuario
    @massa ||= MassadeDados.new
  end

  def usuario
    usuario_existente = Usuarios.new
  end

  def cadastro_login
    @cad_generico ||= CadastroGenerico.new
  end

# Redefinicao de senha (email válido)
def esqueci_minha_senha_para_redefinicao(dados)
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "forgot-password-link").click
  @driver.find_element(:id, "forgot-password-email").send_keys "pablo_costa@moreiramoraes.net"
  @driver.find_element(:css, "#forgot-password-form .btn").click
  sleep 10
  assert(@driver.find_element(:css, ".alert-success").text == "Um link de redefinição de senha foi enviado para seu e-mail.")
  @driver.find_element(:css, ".modal-lonely-close").click
end


### Início dos testes

# Método de validação de login com sucesso na tela principal da loja.
  def test_login_loja_sucesso
    puts "O cenário a seguir vai tratar o Login do cliente com sucesso!!!!"
    usuario.existente(@driver)
    sleep 10
    @verificalogin = @driver.find_element(:id, "user-name-link").text
    p "Você está logado com usuário: " + @verificalogin
    assert(@driver.find_element(:id, "user-name-link").text == "thiago")
  end

  # Método de validação de login com sucesso comprando um item na loja.
  def test_login_compra_sucesso
    puts "O cenário a seguir vai tratar o Login do cliente com sucesso a partir de uma compra na loja!!!!"
    @driver.get(@base_url)
    @driver.find_element(:css, "button.close").click
    ### Aqui o cliente sempre vai clicar no primeiro item da lista do carrossel.
    ### Como não estamos testando o fluxo de compra, não vi problema de sempre setar o primeiro item da lista.
    item = @driver.find_elements(:xpath, "//button[@data-ga-action='clique-comprar']").size
    busca = 0
    @driver.find_elements(:xpath, "//button[@data-ga-action='clique-comprar']")[busca].click
    sleep 10
  # Aqui volta o fluxo de Login através de uma compra.
    @driver.find_element(:id, "login_email").send_keys "a@b.com.br"
    @driver.find_element(:id, "login_password").send_keys "inicial1234"
    @driver.find_element(:id, "submit-signin").click
    sleep 10
    assert(@driver.find_element(:id, "user-name-link").text == "thiago")
  end

# Método de validação de login sem informar a senha.
  def test_login_loja_sem_senha
    puts "O cenário a seguir vai tratar o Login sem informar a senha. Nesse caso, deverá alertar o cliente que
    deverá informar a senha."
    @driver.get(@base_url)
    @driver.find_element(:css, "button.close").click
    @driver.find_element(:id, "header-login-link").click
    @driver.find_element(:id, "login_email").send_keys "a@b.com.br"
    @driver.find_element(:id, "login_password").send_keys ""
    @driver.find_element(:id, "submit-signin").click
    assert(@driver.find_element(:css, "label.error").text == "A senha deve ser preenchida")
  end  

# Método de validação de login inválido.
  def test_login_loja_erro
    puts "O cenário a seguir vai tratar o Login inválido do cliente"
    @driver.get(@base_url)
    @driver.find_element(:css, "button.close").click
    @driver.find_element(:id, "header-login-link").click
    @driver.find_element(:id, "login_email").send_keys "12345t@qdfg.com.br"
    @driver.find_element(:id, "login_password").send_keys "inicial1234"
    @driver.find_element(:id, "submit-signin").click
    sleep 05
    assert(@driver.find_elements(:css, ".errors.hide.clear li")[0].text == "O e-mail ou senha está incorreto.")
  end

# Método de validação de esqueci minha senha com email válido.
  def test_esqueci_minha_senha
    puts "Validação de funcionalidade de esqueci minha senha quando eu informo um email válido."
    @driver.get(@base_url)
    @driver.find_element(:css, "button.close").click
    @driver.find_element(:id, "header-login-link").click
    @driver.find_element(:id, "forgot-password-link").click
    @driver.find_element(:id, "forgot-password-email").send_keys "qa310301@iba.com"
    @driver.find_element(:css, "#forgot-password-form .btn").click
    sleep 10
    assert(@driver.find_element(:css, ".alert.alert-success.hide p").text == "Um link de redefinição de senha foi enviado para seu e-mail.")
end
# Fim do método de validação de senha

# Método de alteracao de senha através do link de redefinir senha
def test_altera_senha_link_alteracao
  puts "Fluxo de validação de alteração de senha através do link de alteração"
  cadastro_login.cadastro_cliente(@driver, @base_url)
  dados = cadastro_login.massausuario 
  ### Desloga ####
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[3].click
  ### Tenta logar e pede para enviar link de redefinição de senha.
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "forgot-password-link").click
  @driver.find_element(:id, "forgot-password-email").send_keys cadastro_login.email
  @driver.find_element(:css, "#forgot-password-form .btn").click
  sleep 10
  assert(@driver.find_element(:css, ".alert.alert-success.hide p").text == "Um link de redefinição de senha foi enviado para seu e-mail.")
  ### Validar o link de definição
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys dados.email
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text == "Como solicitado, segue abaixo o link para redefinir sua senha no iba:") 
  ### A partir desse ponto, iremos validar   
  @link_altera_senha = @driver.find_element(:xpath, "/html/body/div[2]/div/p[3]/a").text
  sleep 10
  @driver.get @link_altera_senha
  assert(@driver.find_element(:css, "div.controls.muted.bold").text == dados.email)
  @driver.find_element(:id, "password").send_keys "inicial12345"
  @driver.find_element(:css, "button.btn.btn-info").click
  sleep 10
  assert(@driver.find_element(:css, ".control-group div.controls label.error").text == "As senhas não coincidem. Por favor, digite novamente.")
  @driver.find_element(:id, "confirm_password").send_keys "inicial12345"
  @driver.find_element(:css, "button.btn.btn-info").click
  sleep 10
  assert(@driver.find_element(:css, "#notifications .flash.success").text == "Você atualizou sua senha com sucesso. Por favor faça o login no iba com a sua nova senha")
  @driver.find_element(:id, "login_email").send_keys dados.email
  @driver.find_element(:id, "login_password").send_keys "inicial12345"
  @driver.find_element(:id, "submit-signin").click
  assert(@driver.find_element(:id, "user-name-link").text == dados.nome)
  end
# Fim do método de alteracao de senha através do link de redefinicao

# Método de validação de esqueci minha senha com email inválido.
  def test_esqueci_minha_senha_invalido
    puts "Validação de funcionalidade de esqueci minha senha quando informo um email inválido."
    @driver.get(@base_url)
    @driver.find_element(:css, "button.close").click
    @driver.find_element(:id, "header-login-link").click
    @driver.find_element(:id, "forgot-password-link").click
    @driver.find_element(:id, "forgot-password-email").send_keys "qa310301@iba"
    @driver.find_element(:css, "#forgot-password-form .btn").click
    assert(@driver.find_element(:css, "#forgot-password-form .clear div.email label.error").text == "Por favor, informe um e-mail válido.")
  end

# Método de validação de Captcha
def test_valida_captcha
  test_login_loja_erro
  sleep 05
  @driver.find_element(:id, "login_password").send_keys "inicial1234"
  @driver.find_element(:id, "submit-signin").click
  sleep 05
  @driver.find_element(:id, "login_password").send_keys "inicial1234"
  @driver.find_element(:id, "submit-signin").click
  sleep 05
# Aqui eu valido se o captcha está presente na tela
  assert(!@driver.find_element(:css, "#recaptcha_image").nil?)
# Agora eu faço um teste onde valido a inserção de um captcha inválido só para validar a mensagem de erro de captcha.
  @driver.find_element(:css, ".recaptcha_input_area input#recaptcha_response_field").send_keys "qwe123"
  @driver.find_element(:id, "login_password").send_keys "inicial1234"
  @driver.find_element(:id, "submit-signin").click
  sleep 10
  assert(@driver.find_elements(:css, ".errors.hide.clear")[0].text == "Os caracteres inseridos não correspondem à verificação de palavras. Tente novamente")
end
# Fim do método de validação de captcha
end
