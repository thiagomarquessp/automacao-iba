# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects
### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.
### Mais um para se basear em melhores práticas: http://ithaca.arpinum.org/2010/07/29/ruby-dynamic-includes.html
### Para executar algum método específico, utilizar o seguinte comando: ruby -I"lib:test" ~/Desktop/Automacaoo/loja/webstore.rb -n test_cadastro_cliente
###  sales@froglogic.com.

require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require "faker"
require "faker_credit_card"
require '../classes/scripts'
require '../classes/massausuario'
require '../classes/pagamentos'

class NovaCompraRevista < Test::Unit::TestCase
	def setup
		@driver = Selenium::WebDriver.for :firefox
    	@base_url = "http://store-qa.ibacloud.com.br/"
    	@accept_next_alert = true
    	@driver.manage.timeouts.implicit_wait = 60
    	@verification_errors = []
	end

  def teardown
    @driver.quit
    assert_equal [], @verification_errors
  end
  
  def element_present?(how, what)
    @driver.find_element(how, what)
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end
  
  def alert_present?()
    @driver.switch_to.alert
    true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  end
  
  def verify(&blk)
    yield
  rescue Test::Unit::AssertionFailedError => ex
    @verification_errors << ex
  end
  
  def close_alert_and_get_its_text(how, what)
    alert = @driver.switch_to().alert()
    alert_text = alert.text
    if (@accept_next_alert) then
      alert.accept()
    else
      alert.dismiss()
    end
    alert_text
  ensure
    @accept_next_alert = true
  end

  def  scripts_initialize
    scripts = ScriptsJs.new
  end

  def  massausuario
      @massa ||= MassadeDados.new
  end

  def usuario
    usuario_existente = Usuarios.new
  end

  def validahistorico
    @history ||= Historicos.new
  end

  def cadastro_generico_nova_compra
    @cad_generico ||= CadastroGenerico.new
  end

def pagamentos_new(dados)
  @pagamento ||= Pagamentos.new(@driver, dados)
end

def calcula_desconto_new
# Método de calculo de desconto do voucher. O valor por hora será e 12%.    
  @buscavalor = @driver.find_element(:xpath, "/html/body/div[4]/div/div[2]/div/div/div/p/strong").text
  @valor = @buscavalor.match( /(\d+,\d+)/ ).captures.first
  @valor = @valor.gsub(',', '.')
  @des = @valor.to_f*0.12
  @total = (@valor.to_f - @des).round(2)
  @total = @total.to_s
# Fim do cálculo.
end


# Método de login com usuário existente
def user_existente
 @driver.get "https://store-qa.ibacloud.com.br/"
 @driver.find_element(:css, "button.close").click
 @driver.find_element(:id, "header-login-link").click
 @driver.find_element(:id, "login_email").send_keys "user_exist@iba.com"
 @driver.find_element(:id, "login_password").send_keys "inicial1234"
 @driver.find_element(:id, "submit-signin").click
end
### Fim do Método de Usuário existente.

### Início dos testes
### Método de nova compra de revistas.
def test_nova_compra_revistas
  puts "Fluxo principal de nova compra de revitas."
  user_existente
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "card_type_visa_online").click
  @driver.find_element(:css, "#main-content .btn.btn-info").click
  @driver.switch_to.frame("global-collect-frame")
  @credit_card_rv = Faker::CreditCard::Visa.number(length: 16)
  @driver.find_element(:id, "F1009").send_keys @credit_card_rv
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:id, "btnSubmit").click
  sleep 10
end
### Fim do método de Novas Compras ###

## Nova compra com cliente One Click Buy e utilizando pela primeira vez o voucher.

def test_utiliza_voucher_novacompra_ocb
  cadastro_generico_nova_compra.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_nova_compra.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos_new(dados)
  @driver.find_element(:id, "save-card").click
  sleep 03
  @driver.find_element(:css, "#save-card-form .btn").click  
  sleep 05
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  #@driver.find_element(:id, "coupon_code").send_keys cadastro_generico_nova_compra.cupom
### Agora eu calculo o voucher.
  calcula_desconto_new
  @driver.find_element(:id, "coupon_code").send_keys cadastro_generico_nova_compra.cupom
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
### Aqui eu transformo o valor total em uma string para validar o teste.
  @validapreco = @driver.find_elements(:css, ".price-placeholder")[1].text
  @validapreco = @validapreco.match( /(\d+,\d+)/ ).captures.first
  @validapreco = @validapreco.to_s
  @validapreco = @validapreco.gsub(',', '.')
### Aqui eu faço uma comparação entre o valor calculado e o valor mostrado.
  if @validapreco == @total
    @resultado_teste = "O valor do produto está correto."
  else
    @resultado_teste = "O valor do produto está incorreto. Fim do Teste"
  end
  sleep 05
  assert(@resultado_teste == "O valor do produto está correto.")
  @driver.find_element(:css, "#confirm-order-modal-form button.btn.btn-info").click
end
  
end
