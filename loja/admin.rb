# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

###### Podemos utilizar link_text no elemento para poder definir um texto ao invés de utilizar o css ou xpath, conforme o exemplo: @driver.find_element(:link_text, "Alterar e-mail").click

require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require 'faker'
require "faker_credit_card"
require "active_support/core_ext"
require '../classes/scripts'
require '../classes/massausuario'
require '../classes/pagamentos'

class Admin < Test::Unit::TestCase

      attr_reader :nome

### Configurações e chamadas de outros métodos e classes de outros arquivos	
def setup
  @driver = Selenium::WebDriver.for :firefox
 	@base_url = "https://store-qa.ibacloud.com.br/"
 	@accept_next_alert = true
 	@driver.manage.timeouts.implicit_wait = 60
 	@verification_errors = []
end

def teardown
  @driver.quit
  assert_equal [], @verification_errors
end
  
def element_present?(how, what)
  @driver.find_element(how, what)
   	true
  	rescue Selenium::WebDriver::Error::NoSuchElementError
   	false
end
  
def alert_present?()
  @driver.switch_to.alert
   	true
  	rescue Selenium::WebDriver::Error::NoAlertPresentError
   	false
end
  
def verify(&blk)
  yield
  rescue Test::Unit::AssertionFailedError => ex
  @verification_errors << ex
end
  
def close_alert_and_get_its_text(how, what)
  alert = @driver.switch_to().alert()
  alert_text = alert.text
   		if (@accept_next_alert) then
      		alert.accept()
   		else
      		alert.dismiss()
   		end
    		  alert_text
  		ensure
    	   	@accept_next_alert = true
end

def  scripts_initialize
  scripts = ScriptsJs.new
end

def  massausuario
  @massa ||= MassadeDados.new
end

def usuario
  usuario_existente = Usuarios.new
end

def cadastro_generico_admin
  @cad_generico ||= CadastroGenerico.new
end

def pagamentos_cad(dados)
  @pagamento ||= Pagamentos.new(@driver, dados)
end

def cupom_vencido
  puts "Armazena cupom inválido."
  @driver.get "https://admin-qa.ibacloud.com.br"
  @driver.find_element(:id, "administrator_email").send_keys "admin@iba.com"
  @driver.find_element(:id, "administrator_password").send_keys "password"
  @driver.find_element(:css, "#new_administrator.new_administrator input.btn").click
  sleep 05
  @driver.find_element(:link_text, "Campanhas").click
  @driver.find_elements(:css, "#container .campaign-header .actions a.btn")[1].click
  sleep 05
  @cupom_inactive = @driver.find_elements(:css, "#container table.results-list tr td")[5].text
  puts @cupom_inactive
end

#### Início dos testes

### Método de criação de voucher ###
def test_cria_voucher_coletivo
  ### Formatacao de Datas pasra inicio e fim de Campanha ### 
  @iniciocampanha = Time.now
  @fimcampanha = @iniciocampanha + 5.days
  @iniciocampanha_formatado = @iniciocampanha.strftime('%d/%m/%Y %H:%M')
  @fimcampanha_formatado = @fimcampanha.strftime('%d/%m/%Y %H:%M')
  ### Criação de voucher. O valor do voucher é de R$100.00 justamente para poder efetuar o teste deixando o item gratuito. ### 
  puts "Fluxo de validação de criação de voucher para ser utilizado na loja."
  @nome_campanha = "Campanha " + Faker::Bitcoin.address
  @driver.get "https://admin-qa.ibacloud.com.br"
  @driver.find_element(:id, "administrator_email").send_keys "admin@iba.com"
  @driver.find_element(:id, "administrator_password").send_keys "password"
  @driver.find_element(:css, "#new_administrator.new_administrator input.btn").click
  sleep 05
  @driver.find_element(:link_text, "Campanhas").click
  @driver.find_elements(:css, "#container .campaign-header .actions a.btn")[3].click
  sleep 05
  @driver.find_element(:id, "voucher_campaign_form_name").send_keys @nome_campanha
  @driver.find_element(:id, "voucher_campaign_form_type_collective").click
  sleep 10
  @driver.find_element(:id, "voucher_campaign_form_end_date").send_keys @fimcampanha_formatado
  sleep 10
  @driver.find_element(:css, "#ui-datepicker-div.ui-datepicker .ui-datepicker-buttonpane .ui-datepicker-close").click
  @driver.find_element(:id, "voucher_campaign_form_discount_type_amount").click
  @driver.find_element(:id, "voucher_campaign_form_discount_value").send_keys "100"
  #@driver.find_element(:id, "voucher_campaign_form_no_of_coupons").send_keys "1"
  @driver.find_element(:css, "#new_voucher_campaign_form.simple_form input.btn").click
  sleep 10
  @new_cupom = @driver.find_elements(:css, "#container table.results-list tr td").last().text
end

# Complemento Teste da Criação do Voucher
def test_utiliza_voucher
  puts "Fluxo principal de comra de revistas utilizando voucher de desconto."
  test_cria_voucher_coletivo
  cadastro_generico_admin.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_admin.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "user_address_postal_code").send_keys dados.cep
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys dados.cpf
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
  @driver.find_element(:id, "coupon_code").send_keys @new_cupom
  sleep 10
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
  assert(@driver.find_elements(:css, "#confirm-order-form strong.price-placeholder")[1].text == "Grátis")
end
##### Fim do fluxo de criação de criação + utilização dos vouhcers na compra.


### Método de criação de voucher coletivo com 100% de desconto.  ###
def test_cria_voucher_coletivo_porcentual
  puts "Fluxo de criação de voucher coletivo com 100% de desconto a ser utiilizado."
  ### Formatacao de Datas para inicio e fim de Campanha ### 
  @iniciocampanha = Time.now
  @fimcampanha = @iniciocampanha + 5.days
  @iniciocampanha_formatado = @iniciocampanha.strftime('%d/%m/%Y %H:%M')
  @fimcampanha_formatado = @fimcampanha.strftime('%d/%m/%Y %H:%M')
  ### Criação de voucher. O valor do voucher é de R$100.00 justamente para poder efetuar o teste deixando o item gratuito. ### 
  puts "Fluxo de validação de criação de voucher para ser utilizado na loja."
  @nome_campanha = "Campanha " + Faker::Bitcoin.address
  @driver.get "https://admin-qa.ibacloud.com.br"
  @driver.find_element(:id, "administrator_email").send_keys "admin@iba.com"
  @driver.find_element(:id, "administrator_password").send_keys "password"
  @driver.find_element(:css, "#new_administrator.new_administrator input.btn").click
  sleep 05
  @driver.find_element(:link_text, "Campanhas").click
  @driver.find_elements(:css, "#container .campaign-header .actions a.btn")[3].click
  sleep 05
  @driver.find_element(:id, "voucher_campaign_form_name").send_keys @nome_campanha
  @driver.find_element(:id, "voucher_campaign_form_type_collective").click
  sleep 10
  @driver.find_element(:id, "voucher_campaign_form_end_date").send_keys @fimcampanha_formatado
  sleep 10
  @driver.find_element(:css, "#ui-datepicker-div.ui-datepicker .ui-datepicker-buttonpane .ui-datepicker-close").click
  @driver.find_element(:id, "voucher_campaign_form_discount_type_percentage").click
  @driver.find_element(:id, "voucher_campaign_form_discount_value").send_keys "100"
  #@driver.find_element(:id, "voucher_campaign_form_no_of_coupons").send_keys "1"
  @driver.find_element(:css, "#new_voucher_campaign_form.simple_form input.btn").click
  sleep 10
  @new_cupom_descont = @driver.find_elements(:css, "#container table.results-list tr td").last().text
  puts @new_cupom_descont
end

### Utilização do voucher de 100% em uma compra. ### 
def test_utiliza_voucher_total
  puts "Fluxo principal de comra de revistas utilizando voucher de desconto."
  cria_voucher_coletivo_porcentual
  cadastro_generico_admin.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_admin.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[1].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "user_address_postal_code").send_keys dados.cep
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys dados.cpf
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
  @driver.find_element(:id, "coupon_code").send_keys @new_cupom_descont
  sleep 10
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
  assert(@driver.find_elements(:css, "#confirm-order-form strong.price-placeholder")[1].text == "Grátis")
  ### Efetuar o pagamento desse item.
  @driver.find_element(:css, "button.btn.btn-info").click
  sleep 10
end
##### Fim do fluxo de Utilização do voucher de 100% em uma compra.

##### Tentativa de utilização de voucher já utilizado em uma compra em uma segunda compra.
def test_reutiliza_voucher
  puts "Fluxo de validação de reutilização de voucher em produtos."
  utiliza_voucher_total
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[1].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 05
  @driver.find_element(:id, "coupon_code").send_keys @new_cupom_descont
  sleep 05
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 05  
  assert(@driver.find_element(:css, ".voucher-panel .voucher-form .voucher-error").text == "Cupom inválido.")
end
#### Fim do cenário de testes onde eu tento utilizar o voucher por mais de uma vez. #####


#### Cenário onde eu tento utilizar um cupom já vencido nas compras .... #######
def test_utiliza_cupom_vencido
  puts "Fluxo de tentativa de utilizacao de um cupom já expirado."
  cupom_vencido
  cadastro_generico_admin.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_admin.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[1].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "user_address_postal_code").send_keys dados.cep
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys dados.cpf
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
  @driver.find_element(:id, "coupon_code").send_keys @cupom_inactive
  sleep 10
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
  assert(@driver.find_element(:css, ".voucher-panel label.voucher-error").text == "Cupom inválido.")
end
#### Fim do Cenário onde eu tento utilizar um cupom já vencido nas compras .... #######

#### Método de criação de voucher para dar desconto apenas em livros.
def test_cria_voucher_desc_ebooks
  puts "Fluxo de validacao de criacao de voucher para desconto apenas em ebooks."
  ### Formatacao de Datas pasra inicio e fim de Campanha ### 
  @iniciocampanha_desc = Time.now
  @fimcampanha_desc = @iniciocampanha_desc + 5.days
  @iniciocampanha_formatado_desc = @iniciocampanha_desc.strftime('%d/%m/%Y %H:%M')
  @fimcampanha_formatado_desc = @fimcampanha_desc.strftime('%d/%m/%Y %H:%M')
  ### Criação de voucher. O valor do voucher é de R$100.00 justamente para poder efetuar o teste deixando o item gratuito. ### 
  puts "Fluxo de validação de criação de voucher para ser utilizado na loja."
  @nome_campanha = "Campanha " + Faker::Bitcoin.address
  @driver.get "https://admin-qa.ibacloud.com.br"
  @driver.find_element(:id, "administrator_email").send_keys "admin@iba.com"
  @driver.find_element(:id, "administrator_password").send_keys "password"
  @driver.find_element(:css, "#new_administrator.new_administrator input.btn").click
  sleep 05
  @driver.find_element(:link_text, "Campanhas").click
  @driver.find_elements(:css, "#container .campaign-header .actions a.btn")[3].click
  sleep 05
  @driver.find_element(:id, "voucher_campaign_form_name").send_keys @nome_campanha
  @driver.find_element(:id, "voucher_campaign_form_type_collective").click
  sleep 10
  @driver.find_element(:id, "voucher_campaign_form_end_date").send_keys @fimcampanha_formatado_desc
  sleep 10
  @driver.find_element(:css, "#ui-datepicker-div.ui-datepicker .ui-datepicker-buttonpane .ui-datepicker-close").click
  @driver.find_element(:id, "voucher_campaign_form_discount_type_amount").click
  @driver.find_element(:id, "voucher_campaign_form_discount_value").send_keys "100"
  @driver.find_elements(:css, ".actions .btn.btn-mini.btn-primary")[1].click
  sleep 10
  @driver.find_element(:id, "voucher_campaign_form_allowed_product_types_book").click
  @driver.find_elements(:css, ".actions .btn.btn-success")[1].click
  sleep 05
  @driver.find_element(:css, "#new_voucher_campaign_form.simple_form input.btn").click
  sleep 10
  @new_cupom = @driver.find_elements(:css, "#container table.results-list tr td").last().text
end

def test_utiliza_voucher_produto
  puts "luxo de utilizacao de voucher criado para ebooks no produto Revistas."
  test_cria_voucher_desc_ebooks
  cadastro_generico_admin.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_admin.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "user_address_postal_code").send_keys dados.cep
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys dados.cpf
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
  ### Aqui eu tento utilizar o cupom criado para ebooks no produto Revistas.
  @driver.find_element(:id, "coupon_code").send_keys @new_cupom
  sleep 10
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
  assert(@driver.find_element(:css, ".voucher-panel label.voucher-error").text == "Uso indevido de cupom.")
end   

def test_utiliza_voucher_produto_certo
  puts "Fluxo de utilizacao de voucher criado para ebooks no produto ebooks."
  test_cria_voucher_desc_ebooks
  cadastro_generico_admin.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_admin.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[1].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "user_address_postal_code").send_keys dados.cep
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys dados.cpf
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
  ### Aqui eu tento utilizar o cupom criado para ebooks no produto Revistas.
  @driver.find_element(:id, "coupon_code").send_keys @new_cupom
  sleep 10
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
  assert(@driver.find_elements(:css, "#confirm-order-form strong.price-placeholder")[1].text == "Grátis")
end

#### Fim do Método de criação de voucher para dar desconto apenas em livros.  
end




































