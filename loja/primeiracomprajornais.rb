# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects
### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.
### Mais um para se basear em melhores práticas: http://ithaca.arpinum.org/2010/07/29/ruby-dynamic-includes.html
### Para executar algum método específico, utilizar o seguinte comando: ruby -I"lib:test" ~/Desktop/Automacaoo/loja/webstore.rb -n test_cadastro_cliente
###  sales@froglogic.com.

require "json"
require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require '../classes/scripts'
require '../classes/massausuario'
require '../classes/pagamentos'

class CompraJornais <Test::Unit::TestCase

def setup
  @driver = Selenium::WebDriver.for :firefox
  @base_url = "http://store-qa.ibacloud.com.br/"
  @accept_next_alert = true
  @driver.manage.timeouts.implicit_wait = 60
  @verification_errors = []
end

def teardown
  @driver.quit
  assert_equal [], @verification_errors
end
  
def element_present?(how, what)
  @driver.find_element(how, what)
  true
  rescue Selenium::WebDriver::Error::NoSuchElementError
  false
end
  
def alert_present?()
  @driver.switch_to.alert
  true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
  false
end
  
def verify(&blk)
  yield
  rescue Test::Unit::AssertionFailedError => ex
  @verification_errors << ex
end
  
def close_alert_and_get_its_text(how, what)
   alert = @driver.switch_to().alert()
   alert_text = alert.text
   if (@accept_next_alert) then
   alert.accept()
   else
   alert.dismiss()
   end
   alert_text
   ensure
   @accept_next_alert = true
end

def  scripts_initialize
  scripts = ScriptsJs.new
end

#  def  massausuario
#      @massa ||= MassadeDados.new
#  end

def usuario
  usuario_existente = Usuarios.new
end

def pagamentos(dados)
  @pagamento ||= Pagamentos.new(@driver, dados)
end

def validahistorico
  @history ||= Historicos.new
end

def cadastro_generico_compra
  @cad_generico ||= CadastroGenerico.new
end

# Método de Busca para comprar pela capa.
def random_sinopse_jornais
  revistas = @driver.find_elements(:css, "a.newspaper-edition-cover") 
  sinopse = revistas.size
  busca_sinopse = Random.rand(sinopse)
  revistas[busca_sinopse].click
end

def calcula_desconto_jornais
# Método de calculo de desconto do voucher. O valor por hora será e 12%.    
  @buscavalor = @driver.find_element(:xpath, "/html/body/div[3]/div[2]/section/div[2]/form/fieldset/div[2]/div/div[3]/p/strong").text
  @valor = @buscavalor.match( /(\d+,\d+)/ ).captures.first
  @valor = @valor.gsub(',', '.')
  @des = @valor.to_f*0.12
  @total = (@valor.to_f - @des).round(2)
  @total = @total.to_s
# Fim do cálculo.
end

### Início dos testes
### Método de Primeira Compra de Jornais com valor. ###
def test_primeira_compra_jornais
  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_compra.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[2].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos(dados)
end
### Fim do Método de Primeira Compra de Jornais com valor. ###

### Método de compra de Revistas quando eu seleciono uma revista.
def test_primeira_compra_sinopse_jornais
  puts "Fluxo de compra de revistas principal quando eu vejo do que se trata uma jornais."
  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_compra.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[2].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  random_sinopse_jornais
  @driver.find_element(:css, ".purchase button.btn").click
  sleep 10
  pagamentos(dados)
end
### Fim do método de compra

### Método de Primeira Compra de Jornais utilizando voucher de desconto.###
def test_primeira_compra_jornais_voucher
  puts "Fluxo principal de comra de jornais utilizando voucher de desconto."
  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_compra.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[2].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  @driver.find_element(:id, "user_address_postal_code").send_keys dados.cep
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys dados.cpf
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
### Agora eu calculo o voucher.
  calcula_desconto_jornais
  @driver.find_element(:id, "coupon_code").send_keys cadastro_generico_compra.cupom
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
### Aqui eu transformo o valor total em uma string para validar o teste.
  @validapreco = @driver.find_element(:css, "form#confirm-order-form.confirm-order-form fieldset.product-details-container div.left div.product-details div.price-info p.price-with-discount span.highlight-fade strong.price-placeholder").text
  @validapreco = @validapreco.match( /(\d+,\d+)/ ).captures.first
  @validapreco = @validapreco.to_s
  @validapreco = @validapreco.gsub(',', '.')
### Aqui eu faço uma comparação entre o valor calculado e o valor mostrado.
  if @validapreco == @total
  then
  puts "O valor do produto está correto."
  else
  puts "O teste está inválido."
  end
  sleep 05
  assert(@driver.find_element(:css, ".voucher-display div.voucher-display-message").text == "Após selecionar \"Efetuar Pagamento\" você não poderá remover o cupom")
  @driver.find_element(:id, "card_type_visa_online").click
  sleep 10
  @driver.find_element(:css, "#main-content .btn.btn-info").click
  @driver.switch_to.frame("global-collect-frame")
  @driver.find_element(:css, "#F1009").send_keys "4242424242424242"
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:css, "#btnSubmit").click
  sleep 20
end

### Método de Primeira Compra de Jornais Gratuita ###
#  def test_primeira_compra_jornais_gratuitos
#    cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
#    @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[2].click
#    @driver.find_element(:id, "show_more_link").click
#    @driver.find_element(:id, "show_more_link").click
#    scripts_initialize.select_random_product_for(@driver, "resgatar-gratis")
#    sleep 05
#    assert(@driver.find_element(:css, "#order-success-modal div.modal-header h3").text == "O seu pedido foi finalizado com sucesso.")
#    @driver.find_element(:css, "#order-success-modal div.modal-footer button.btn.btn-info").click
#  end
### Fim do Método de Primeira Compra de Jornais ###
end
