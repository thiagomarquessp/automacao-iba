# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects
### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.
### Mais um para se basear em melhores práticas: http://ithaca.arpinum.org/2010/07/29/ruby-dynamic-includes.html
### Para executar algum método específico, utilizar o seguinte comando: ruby -I"lib:test" ~/Desktop/Automacaoo/loja/webstore.rb -n cadastro_cliente
###  sales@froglogic.com.


###### Podemos utilizar link_text no elemento para poder definir um texto ao invés de utilizar o css ou xpath, conforme o exemplo: @driver.find_element(:link_text, "Alterar e-mail").click

require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require 'faker'
require "faker_credit_card"
require '../classes/scripts'
require '../classes/massausuario'
require '../classes/pagamentos'

class AlteraCadastro < Test::Unit::TestCase

  

### Configurações e chamadas de outros métodos e classes de outros arquivos	
  def setup
	  @driver = Selenium::WebDriver.for :firefox
 	  @base_url = "https://store-qa.ibacloud.com.br/"
 	  @accept_next_alert = true
 	  @driver.manage.timeouts.implicit_wait = 60
 	  @verification_errors = []
	end

  def teardown
   	@driver.quit
   	assert_equal [], @verification_errors
  end
  
  def element_present?(how, what)
   	@driver.find_element(how, what)
   	true
  	rescue Selenium::WebDriver::Error::NoSuchElementError
   	false
  end
  
  def alert_present?()
   	@driver.switch_to.alert
   	true
  	rescue Selenium::WebDriver::Error::NoAlertPresentError
   	false
  end
  
  def verify(&blk)
   	yield
  	rescue Test::Unit::AssertionFailedError => ex
   	@verification_errors << ex
  end
  
  def close_alert_and_get_its_text(how, what)
   	alert = @driver.switch_to().alert()
   	alert_text = alert.text
   		if (@accept_next_alert) then
      		alert.accept()
   		else
      		alert.dismiss()
   		end
    		  alert_text
  		ensure
    	   	@accept_next_alert = true
  end

  def  scripts_initialize
   	scripts = ScriptsJs.new
  end

  def  massausuario
    @massa ||= MassadeDados.new
  end

  def usuario
   	usuario_existente = Usuarios.new
  end

  def cadastro_generico_altera
    @cad_generico ||= CadastroGenerico.new
  end

def pagamentos_cad(dados)
  @pagamento ||= Pagamentos.new(@driver, dados)
end

def calcula_desconto
# Método de calculo de desconto do voucher. O valor por hora será e 12%.    
  @buscavalor = @driver.find_element(:xpath, "/html/body/div[3]/div[2]/section/div[2]/form/fieldset/div[2]/div/div[3]/p/strong").text
  @valor = @buscavalor.match( /(\d+,\d+)/ ).captures.first
  @valor = @valor.gsub(',', '.')
  @des = @valor.to_f*0.12
  @total = (@valor.to_f - @des).round(2)
  @total = @total.to_s
# Fim do cálculo.
end


### Método de Primeira Compra para cadastro completo. ###
def cad_completo
  puts "Fluxo principal de compra de revistas com valor sem One Click Buy."
  cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_altera.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos_cad(dados)
end
### Fim do Método de Primeira Compra para cadastro completo ###

### Cartão de crédito SALVO###
def cartao_salvo
  puts "Fluxo princiapl de compra de revistas com cliente One Click Buy."
  cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_altera.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos_cad(dados)
  @driver.find_element(:id, "save-card").click
  sleep 03
  @driver.find_element(:css, "#save-card-form .btn").click  
  sleep 05
end
### Fim do Método de Primeira Compra de Revistas ###


### Aqui se inicia os testes de Cadastro
### Contexto: Com o objetivo de aumentar a cobertura de testes do Fluxo de Cadastro de Usuário, 
### assim como, automatizar o mesmo, devemos iniciar a criação dos scripts dos cenários de testes já definidos.


# Método validação de tela de cadastro e edição de registros.
  def test_valida_campos_edicao
    puts "Fluxo de edição de cadasto de usuário após cadastro."
    cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    confirmaemail = @driver.find_elements(:css, ".user-info-fields dd")[1].text
    #"A partir daqui vamos alterar o email do cliente por um email válido"#
    puts "A partir daqui vamos alterar o email do cliente por um email válido"
    @driver.find_element(:css, "#profile-container .btn").click
    @driver.find_element(:id, "change-email-link").click
    assert(@driver.find_element(:css, ".email-address").text == confirmaemail)
    assert(@driver.find_element(:id, "new_email").text == "")
    assert(@driver.find_element(:id, "confirm_email").text == "") 
    assert(@driver.find_element(:id, "password").text == "") 
  end

# Método de validação de alteração de email com email válido.
  def test_valida_alteracao_email
    puts "Fluxo de edição/alteração de email."
    test_valida_campos_edicao
    massausuario.fake_generator(@driver)
    @driver.get(@base_url)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click      
    @driver.find_element(:id, "change-email-link").click
    @driver.find_element(:id, "new_email").send_keys massausuario.email
    @driver.find_element(:id, "confirm_email").send_keys massausuario.email
    @driver.find_element(:id, "password").send_keys "inicial1234"
    @driver.find_element(:css, "#change-email-form .btn").click
    sleep 05
    assert(@driver.find_element(:css, "#notifications").text == "E-mail alterado com sucesso. Um e-mail será enviado com os detalhes para verificação do cadastro.")
  end

# Método de validação de campos vazios.
  def test_valida_campos_brancos
    puts "Fluxo de validação de edição com campos vazios."
    cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click      
    @driver.find_element(:id, "change-email-link").click
    @driver.find_element(:css, "#change-email-form .btn").click
    @driver.find_element(:css, "#change-email-form .new-email label.error").text
    assert(@driver.find_element(:css, "#change-email-form .new-email label.error").text == "O e-mail deve ser preenchido.")
    assert(@driver.find_element(:css, "#change-email-form fieldset.clear .confirm-email label.error").text == "O e-mail deve ser preenchido.")
    assert(@driver.find_element(:css, "#change-email-form fieldset.clear .password label.error").text == "A senha deve ser preenchida.")
  end

# Método de validação de email já existente na base.
  def test_valida_edicao_email_existente
    puts "Fluxo de alteração de email, dado que o email alterado já existe na base."
    usuario.existente(@driver)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click      
    @driver.find_element(:id, "change-email-link").click
    @driver.find_element(:id, "new_email").send_keys "teste@webstore.com.br"
    @driver.find_element(:id, "confirm_email").send_keys "teste@webstore.com.br"
    @driver.find_element(:id, "password").send_keys "inicial1234"    
    @driver.find_element(:css, "#change-email-form .btn").click
    assert(@driver.find_element(:css, "#change-email-form .new-email label.error").text == "Este e-mail já está cadastrado.")
  end

# Método de validação de emails diferentes na hora da edição.
  def test_valida_edicao_emails_diferentes
    puts "Fluxo de validação de alteração de emails com emails diferentes."
    usuario.existente(@driver)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click      
    @driver.find_element(:id, "change-email-link").click
    @driver.find_element(:id, "new_email").send_keys "alteraemail@webstore.com"
    @driver.find_element(:id, "confirm_email").send_keys "alteraemail@webstore.com.br"
    @driver.find_element(:id, "password").send_keys "inicial1234"    
    @driver.find_element(:css, "#change-email-form .btn").click
    assert(@driver.find_element(:css, "#change-email-form fieldset.clear .confirm-email label.error").text == "O campo \"Confirmar Email\" deve ser igual ao campo \"Email\"")
  end

# Método de validação de alteração de senha.
  def test_valida_alteracao_senha
    puts "Fluxo de validação de alteração de senha."
    cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click 
    @driver.find_element(:id, "change-password-link").click
    # A partir daqui aparece o modal de alteração de senha.
    # Nesse caso, temos que informar a senha atual e informar nova senha.
    @driver.find_element(:css, "#change-password .modal-body #current_password").send_keys "inicial1234"
    @driver.find_element(:id, "new_password").send_keys "inicial12345"
    @driver.find_element(:id, "password_confirmation").send_keys "inicial12345"
    @driver.find_element(:css, "#change-password-form .action-buttons .btn").click
    sleep 05
    assert(@driver.find_element(:css, ".container .success").text == "Senha alterada com sucesso. Um e-mail será enviado com os detalhes para verificação.")
    assert(@driver.find_element(:id, "user-name-link").text == cadastro_generico_altera.nome)
  ### Print da tela com o resultado esperado.    
    time = Time.new
    @driver.save_screenshot(File.dirname('/Users/Vendaval/Desktop/screenshots') + '/screenshots/' + @method_name + '_' + time.strftime('%Y%m%d_%H%M%S') + '.png')    
  end 
### Fim do Método de validação de alteração de senha.

## Método de utilizacao de voucher após alterar a senha e logar com a nova senha.
def test_utiliza_voucher
  puts "Utilização de voucher após alteração de senha."
  test_valida_alteracao_senha
  ### Desloga ####
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[3].click
  ### Loga com nova senha ###
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "login_email").send_keys cadastro_generico_altera.email
  @driver.find_element(:id, "login_password").send_keys "inicial12345"
  @driver.find_element(:id, "submit-signin").click
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  @driver.find_element(:id, "user_address_postal_code").send_keys "04548-000"
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys Faker::CPF.numeric
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
### Agora eu calculo o voucher.
  calcula_desconto
  @driver.find_element(:id, "coupon_code").send_keys cadastro_generico_altera.cupom
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
### Aqui eu transformo o valor total em uma string para validar o teste.
  @validapreco = @driver.find_element(:css, "form#confirm-order-form.confirm-order-form fieldset.product-details-container div.left div.product-details div.price-info p.price-with-discount span.highlight-fade strong.price-placeholder").text
  @validapreco = @validapreco.match( /(\d+,\d+)/ ).captures.first
  @validapreco = @validapreco.to_s
  @validapreco = @validapreco.gsub(',', '.')
### Aqui eu faço uma comparação entre o valor calculado e o valor mostrado.
  if @validapreco == @total
  then
  puts "O valor do produto está correto."
  else
  puts "O teste está inválido."
  end
  sleep 05
  assert(@driver.find_element(:css, ".voucher-display div.voucher-display-message").text == "Após selecionar \"Efetuar Pagamento\" você não poderá remover o cupom")
  @driver.find_element(:id, "card_type_visa_online").click
  sleep 10
  @driver.find_element(:css, "#main-content .btn.btn-info").click
  @driver.switch_to.frame("global-collect-frame")
  @driver.find_element(:css, "#F1009").send_keys "4242424242424242"
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:css, "#btnSubmit").click
  sleep 20
end
### Fim do Método de utilizacao de voucher após alterar a senha e logar com a nova senha.

# Método de teste de validação de Email de Alteração de Senha
def test_valida_email_alteracao_senha
  puts "Fluxo de Validação de email de Alteração de Senha."
  test_valida_alteracao_senha
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_altera.email
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text == "Sua senha foi alterada com sucesso no iba, a maior loja de conteúdo digital do Brasil.")
end
## Fim do método de validação de Email de Alteração de Senha

### Método de validação de login com nova senha criada.
def test_valida_login_nova_senha
  puts "Fluxo de validação de login quando o cliente altera a senha na loja."
  test_valida_alteracao_senha
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[3].click
  sleep 10
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "login_email").send_keys cadastro_generico_altera.email
  @driver.find_element(:id, "login_password").send_keys "inicial12345"
  @driver.find_element(:id, "submit-signin").click
  assert(@driver.find_element(:id, "user-name-link").text == cadastro_generico_altera.nome)  
end
### Fim do método de validação de login com nova senha criada.

# Método de alteração de senha, validando senhas inválida.
  def test_valida_alteracao_senha_invalida
    puts "Fluxo de validação de alteração de senha informando uma senha inválida (Senha Atual)"
    cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click 
    @driver.find_element(:id, "change-password-link").click
    # A partir daqui aparece o modal de alteração de senha.
    @driver.find_element(:css, "#change-password .modal-body #current_password").send_keys "inicial12345"
    @driver.find_element(:id, "new_password").send_keys "inicial1234"
    @driver.find_element(:id, "password_confirmation").send_keys "inicial1234"
    @driver.find_element(:css, "#change-password-form .action-buttons .btn").click
    sleep 05
    assert(@driver.find_element(:css, "#change-password-form label.error").text == "Senha incorreta. Esqueceu a senha? Clique aqui")
  end

# Método de validação de alteração de senha quando eu informo uma senha que não está nos critérios de segurança.
  def test_valida_alteracao_senha_insegura
    puts "Fluxo de validação de alteração de senha quando a senha não está nos critérios de segurança"
    usuario.existente(@driver)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click 
    @driver.find_element(:id, "change-password-link").click
    # A partir daqui aparece o modal de alteração de senha.
    @driver.find_element(:css, "#change-password .modal-body #current_password").send_keys "inicial1234"
    @driver.find_element(:id, "new_password").send_keys "asdfghjk"
    @driver.find_element(:id, "password_confirmation").send_keys "asdfghjk"
    @driver.find_element(:css, "#change-password-form .action-buttons .btn").click
    assert(@driver.find_elements(:css, "#change-password-form label.error")[1].text == "A senha deve conter pelo menos 8 caracteres, incluindo letras e números.")
  end

# Método de validação de alteração de senha quando as senhas não coincidem.

  def test_valida_alteracao_senha_diferente
    puts "Fluxo de validação de alteração de senha quando eu informo Nova Senha e Confirmar Nova Senha diferentes."
    usuario.existente(@driver)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click 
    @driver.find_element(:id, "change-password-link").click
    # A partir daqui aparece o modal de alteração de senha.
    @driver.find_element(:css, "#change-password .modal-body #current_password").send_keys "inicial1234"
    @driver.find_element(:id, "new_password").send_keys "inicial12345"
    @driver.find_element(:id, "password_confirmation").send_keys "asdfghjk"
    @driver.find_element(:css, "#change-password-form .action-buttons .btn").click
    assert(@driver.find_element(:css, "#change-password-form label.error").text == "Ops! Você precisa preencher os 2 campos com a mesma senha nova")
  end

  def test_valida_tela_meucadastro
    puts "Fluxo de validação de tela de Meu Cadastro (Campos de Endereço devem estar vazios)"
    cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    assert(@driver.find_elements(:css, ".user-info-fields dd")[2].text == "")
    assert(@driver.find_elements(:css, ".address-fields dd")[0].text == "")
    assert(@driver.find_elements(:css, ".address-fields dd")[1].text == "")
    assert(@driver.find_elements(:css, ".address-fields dd")[2].text == "")
    assert(@driver.find_elements(:css, ".address-fields dd")[3].text == "")
    assert(@driver.find_elements(:css, ".address-fields dd")[4].text == "")
    assert(@driver.find_elements(:css, ".address-fields dd")[5].text == "")
    assert(@driver.find_elements(:css, ".address-fields dd")[6].text == "")  
  end

### Método de alteração de dados de endereço.
def test_altera_dados_endereco
  puts "Fluxo de alteração de dados de endereço."
  cad_completo
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click  
  @driver.find_element(:css, "li.postal_code input#user_address_postal_code").clear
  @driver.find_element(:css, "li.postal_code input#user_address_postal_code").send_keys "04548-000"
  @driver.find_element(:id, "user_address_street_number").clear
  sleep 10
  @driver.find_element(:id, "user_address_street_number").send_keys "54321"
  @driver.find_element(:css, ".control-group .btn").click
  sleep 10
  assert(@driver.find_element(:css, "ul.flash.success li").text == "Os detalhes do perfil foram salvos com sucesso")
  ### Print da tela com o resultado esperado.    
    time = Time.new
    @driver.save_screenshot(File.dirname('/Users/Vendaval/Desktop/screenshots') + '/screenshots/' + @method_name + '_' + time.strftime('%Y%m%d_%H%M%S') + '.png')
end
### Fim do método de alteração de dados de endereço.

### Método de alteração de dados de endereço.
def test_altera_nome_cliente
  puts "Fluxo de alteração do nome do cliente cadastrado."
  cad_completo
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click  
  @driver.find_element(:css, ".user-info-fields dd #user_name").clear
  @driver.find_element(:css, ".user-info-fields dd #user_name").send_keys "Teste Altera Nome do Cliente"
  @driver.find_element(:css, ".control-group .btn").click
  sleep 10
  assert(@driver.find_element(:css, "ul.flash.success li").text == "Os detalhes do perfil foram salvos com sucesso")
  ### Print da tela com o resultado esperado.    
    time = Time.new
    @driver.save_screenshot(File.dirname('/Users/Vendaval/Desktop/screenshots') + '/screenshots/' + @method_name + '_' + time.strftime('%Y%m%d_%H%M%S') + '.png')
end
### Fim do método de alteração de dados de nome do cliente.

### Método de exclusao de cartão de crédito.
def test_exclui_cartao_salvo
  cartao_salvo
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click  
  @driver.find_element(:link_text, "remover").click
  sleep 10 
  @driver.find_element(:css, ".modal-body a.btn.btn-danger").click
  sleep 05
  assert(@driver.find_element(:css, "#notifications .flash.success li").text == "O cartão de crédito foi removido com sucesso")
  ### Print da tela com o resultado esperado.    
    time = Time.new
    @driver.save_screenshot(File.dirname('/Users/Vendaval/Desktop/screenshots') + '/screenshots/' + @method_name + '_' + time.strftime('%Y%m%d_%H%M%S') + '.png')  
end
### Fim do método de exclusão de cartão de crédito

### Método de exclusao de cartão de crédito.
def test_nao_exclui_cartao_salvo
  puts "Validaçao de não exclusão de cartão de crédito."
  # Cadastrando e virando One click Buy
  cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_altera.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos_cad(dados)
  @driver.find_element(:id, "save-card").click
  sleep 03
  @driver.find_element(:css, "#save-card-form .btn").click  
  sleep 05
  # Exclusão de cartão.
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click  
  @driver.find_element(:link_text, "remover").click
  sleep 10 
  @driver.find_elements(:css, ".modal-body button.btn")[2].click
  sleep 05
  assert(@driver.find_elements(:css, "#profile-container.left .credit-card-fields dd")[1].text == "XXXX XXXX XXXX " + dados.visa_ultimos_digitos)
  ### Print da tela com o resultado esperado.    
    time = Time.new
    @driver.save_screenshot(File.dirname('/Users/Vendaval/Desktop/screenshots') + '/screenshots/' + @method_name + '_' + time.strftime('%Y%m%d_%H%M%S') + '.png')  
end
### Fim do método de exclusão de cartão de crédito

### Método de validação de confirmação de email quando cliente possui cartão salvo.
def test_confirma_alteracao_email_ocb
  puts "Validar confirmacao de alteracao de email."
  # Cadastrando e virando One click Buy
  cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_altera.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos_cad(dados)
  @driver.find_element(:id, "save-card").click
  sleep 03
  @driver.find_element(:css, "#save-card-form .btn").click  
  sleep 05
  # Alteracao de Email
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click      
  @driver.find_element(:id, "change-email-link").click
  @emailaltera = Faker::Internet.email
  @driver.find_element(:id, "new_email").send_keys @emailaltera
  @driver.find_element(:id, "confirm_email").send_keys @emailaltera
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "#change-email-form .btn").click
  ## Aqui eu valido o email de alteracao de senha
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys @emailaltera
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  @confirmaalteracao = @driver.find_element(:xpath, "/html/body/div[2]/div/p[3]/a").text
  @driver.get @confirmaalteracao
  sleep 05
  assert(@driver.find_element(:css, "#notifications ul.flash li").text == "Seu email foi confirmado com sucesso!")
  @driver.find_element(:id, "user-name-link").click
  # Validar cartão do cliente no novo email.
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click
  assert(@driver.find_elements(:css, "#profile-container.left .credit-card-fields dd")[1].text == "XXXX XXXX XXXX " + dados.visa_ultimos_digitos)
  #assert(@driver.find_elements(:css, "dl.credit-card-fields dt label")[1].text == "Número do cartão")
end
### Fim do Método de validação de confirmação de email quando cliente possui cartão salvo.

### Método de utilizacao de voucher apos alteracao de email.
def test_utiliza_voucher_altera_email
  puts "Validar confirmacao de alteracao de email e utilizacao de voucher com novo email."
  # Cadastrando e virando One click Buy
  cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_altera.massausuario
  # Alteracao de Email
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click      
  @driver.find_element(:id, "change-email-link").click
  @emailaltera = Faker::Internet.email
  @driver.find_element(:id, "new_email").send_keys @emailaltera
  @driver.find_element(:id, "confirm_email").send_keys @emailaltera
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "#change-email-form .btn").click
  ## Aqui eu valido o email de alteracao de senha
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys @emailaltera
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  @confirmaalteracao = @driver.find_element(:xpath, "/html/body/div[2]/div/p[3]/a").text
  @driver.get @confirmaalteracao
  sleep 05
  ### Desloga ####
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[3].click
  ### Loga com nova senha ###
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "login_email").send_keys @emailaltera
  @driver.find_element(:id, "login_password").send_keys "inicial1234"
  @driver.find_element(:id, "submit-signin").click
  ### Realizacao da compra com voucher no novo email.
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "user_address_postal_code").send_keys "04548-000"
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys Faker::CPF.numeric
  @driver.find_element(:id, "user_address_street_number").send_keys "1234"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
### Agora eu calculo o voucher.
  calcula_desconto
  @driver.find_element(:id, "coupon_code").send_keys cadastro_generico_altera.cupom
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
### Aqui eu transformo o valor total em uma string para validar o teste.
  @validapreco = @driver.find_element(:css, "form#confirm-order-form.confirm-order-form fieldset.product-details-container div.left div.product-details div.price-info p.price-with-discount span.highlight-fade strong.price-placeholder").text
  @validapreco = @validapreco.match( /(\d+,\d+)/ ).captures.first
  @validapreco = @validapreco.to_s
  @validapreco = @validapreco.gsub(',', '.')
### Aqui eu faço uma comparação entre o valor calculado e o valor mostrado.
  if @validapreco == @total
  then
  puts "O valor do produto está correto."
  else
  puts "O teste está inválido."
  end
  sleep 05
  assert(@driver.find_element(:css, ".voucher-display div.voucher-display-message").text == "Após selecionar \"Efetuar Pagamento\" você não poderá remover o cupom")
  @driver.find_element(:id, "card_type_visa_online").click
  sleep 10
  @driver.find_element(:css, "#main-content .btn.btn-info").click
  @driver.switch_to.frame("global-collect-frame")
  @driver.find_element(:css, "#F1009").send_keys "4242424242424242"
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:css, "#btnSubmit").click
  sleep 20
end
### Fim do Método de utilizacao de voucher apos alteracao de email.


### Método de alteracao de cartao de credito com cliente Iba CLube
def test_altera_cartao_ibaclube
  @driver.get "https://store-qa.ibacloud.com.br/"
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "login_email").send_keys "pedro@santosmoreira.org"
  @driver.find_element(:id, "login_password").send_keys "inicial1234"
  @driver.find_element(:id, "submit-signin").click
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click  
  @driver.find_element(:css, "#edit-profile-form .edit-card .btn").click
  @novocartao = Faker::CreditCard::Visa.number(length: 16)
  @ultimos_digitos = @novocartao[-4..-1]
  @driver.find_element(:id, "card_type_visa_online").click
  sleep 10
  @driver.switch_to.frame("global-collect-frame")
  @driver.find_element(:id, "F1009").send_keys @novocartao
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:css, "#btnSubmit").click
  sleep 10
  @driver.switch_to.alert.accept
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click
  assert(@driver.find_elements(:css, "#profile-container.left .credit-card-fields dd")[1].text == "XXXX XXXX XXXX " + @ultimos_digitos)
### Print da tela com o resultado esperado.    
    time = Time.new
    @driver.save_screenshot(File.dirname('/Users/Vendaval/Desktop/screenshots') + '/screenshots/' + @method_name + '_' + time.strftime('%Y%m%d_%H%M%S') + '.png')  
end
### Fim do método de alteracao de cartao de credito com cliente Iba Clube

### Método de validação de cancelamento de alteração de email
def test_cancela_alteracao_email
  puts "Cenário de cancelamento de alteração de email."
  cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click      
  @driver.find_element(:id, "change-email-link").click
  @altera_email = Faker::Internet.email
  @driver.find_element(:id, "new_email").send_keys @altera_email
  @driver.find_element(:id, "confirm_email").send_keys @altera_email
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "#change-email-form .btn").click
 ## Aqui eu valido o email de alteracao de senha
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 05
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_altera.email
  sleep 05
  @driver.find_elements(:css, "ul.messages_list li.email div.row")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  @driver.find_element(:xpath, "/html/body/div[2]/div/p[2]/a").click
  @driver.switch_to.window @driver.window_handles.last
  sleep 10
  assert(@driver.find_element(:css, "#main-content .information-container div").text.include?(cadastro_generico_altera.email))
## Agora vou verificar se o email anterior continua o mesmo cadastrado
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click  
  assert(@driver.find_elements(:css, ".user-info-fields dd")[1].text == cadastro_generico_altera.email)
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[3].click   
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "login_email").send_keys cadastro_generico_altera.email
  @driver.find_element(:id, "login_password").send_keys "inicial1234"
  @driver.find_element(:id, "submit-signin").click
end
### Fim do método de validação de cancelamento de alteração de email

### Método de validação de cancelamento de alteração de email
def test_confirma_new_email
  puts "Validar se usuario consegue cancelar a alteracao de email após ativar a alteracao de email."
  cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click      
  @driver.find_element(:id, "change-email-link").click
  @altera_email_confirma = Faker::Internet.email
  @driver.find_element(:id, "new_email").send_keys @altera_email_confirma
  @driver.find_element(:id, "confirm_email").send_keys @altera_email_confirma
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "#change-email-form .btn").click
 ## Aqui eu valido o email de alteracao de senha
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 05
  @driver.find_element(:css, "input.quick_filter").send_keys @altera_email_confirma
  sleep 05
  @driver.find_elements(:css, "ul.messages_list li.email div.row")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  @driver.find_element(:xpath, "/html/body/div[2]/div/p[3]/a").click
  @driver.switch_to.window @driver.window_handles.last
  sleep 10
  assert(@driver.find_element(:css, "#notifications .flash li").text == "Seu email foi confirmado com sucesso!")
  ## Agora vou verificar se foi alterado com sucesso
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click  
  assert(@driver.find_elements(:css, ".user-info-fields dd")[1].text == @altera_email_confirma)
  @driver.switch_to.window @driver.window_handles.first
  ## Agora eu vou voltar para o email anterior
  @driver.find_element(:css, "input.quick_filter").clear()
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_altera.email
  sleep 05
  @driver.find_elements(:css, "ul.messages_list li.email div.row")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  @driver.find_element(:xpath, "/html/body/div[2]/div/p[2]/a").click
  @driver.switch_to.window @driver.window_handles.last
  assert(@driver.find_element(:css, "#main-content .information-container div").text.include?(cadastro_generico_altera.email))
end
### Fim do método de validação de cancelamento de alteração de email

### Validacao de expiracao de link de alteracao de senha.
def test_expiracao_link_redef_senha
  puts "Fluxo de validação de expiração (no caso 5 dias) de link de alteração de senha."
  @driver.get "https://store-qa.ibacloud.com.br/user/forgot_password/xinaf-cyreg-cufih-cigih-hurof-civig-zapyh-cygol-byguh-gekyg-vavek-lozuf-tyxix?email=fbio.albuquerque%40souzafranco.info"
  sleep 10
end
### Fim do método de Validacao de expiracao de link de alteracao de senha.

### Método de compra com cartão alterado.
def test_utiliza_cartao_alterado
  cadastro_generico_altera.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_altera.massausuario 
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos_cad(dados)
### Aqui inicia a compra com o cliente já One Click Buy.
  @driver.find_element(:id, "save-card").click
  sleep 03
  @driver.find_element(:css, "#save-card-form .btn").click
  sleep 10     
### Aqui se altera o cartão de crédito.
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click  
  @driver.find_element(:css, "#edit-profile-form .edit-card .btn").click
  @new_card = Faker::CreditCard::Visa.number(length: 16) 
  sleep 05
  @driver.find_element(:id, "card_type_visa_online").click
  sleep 10
  @driver.switch_to.frame("global-collect-frame")
  @driver.find_element(:id, "F1009").send_keys @new_card
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:css, "#btnSubmit").click
  sleep 10
  @driver.switch_to.alert.accept
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click
### Agora vou fazer a compra com o novo cartão.
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click 
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 05
  pagamentos_cad(dados)
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "#confirm-order-modal-form  button.btn.btn-info").click
  sleep 10
end

### Fim do Método de compra com cartão alterado.


end
