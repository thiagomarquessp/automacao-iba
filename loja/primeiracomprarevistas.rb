# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects
### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.
### Mais um para se basear em melhores práticas: http://ithaca.arpinum.org/2010/07/29/ruby-dynamic-includes.html
### Para executar algum método específico, utilizar o seguinte comando: ruby -I"lib:test" ~/Desktop/Automacaoo/loja/webstore.rb -n cadastro_cliente
###  sales@froglogic.com.

require "json"
require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require '../classes/scripts'
require '../classes/massausuario'
require '../classes/pagamentos'

class CompraRevista <Test::Unit::TestCase

def setup
  @driver = Selenium::WebDriver.for :firefox
  @base_url = "http://store-qa.ibacloud.com.br/"
  @accept_next_alert = true
  @driver.manage.timeouts.implicit_wait = 60
  @verification_errors = []
end

def teardown
  @driver.quit
  assert_equal [], @verification_errors
end
  
def element_present?(how, what)
  @driver.find_element(how, what)
  true
  rescue Selenium::WebDriver::Error::NoSuchElementError
  false
end
  
def alert_present?()
  @driver.switch_to.alert
  true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
  false
end
  
def verify(&blk)
  yield
  rescue Test::Unit::AssertionFailedError => ex
  @verification_errors << ex
end
  
def close_alert_and_get_its_text(how, what)
  alert = @driver.switch_to().alert()
  alert_text = alert.text
  if (@accept_next_alert) then
  alert.accept()
  else
  alert.dismiss()
  end
  alert_text
  ensure
  @accept_next_alert = true
end

def  scripts_initialize
  scripts = ScriptsJs.new
end

#  def  massausuario
#      @massa ||= MassadeDados.new
#  end

def usuario
  usuario_existente = Usuarios.new
end

def pagamentos(dados)
  @pagamento ||= Pagamentos.new(@driver, dados)
end

def validahistorico
  @history ||= Historicos.new
end

def cadastro_generico_compra
  @cad_generico ||= CadastroGenerico.new
end

# Método de Busca para comprar pela capa.
def random_sinopse
  revistas = @driver.find_elements(:css, "a.magazine-issue-cover") 
  sinopse = revistas.size
  busca_sinopse = Random.rand(sinopse)
  revistas[busca_sinopse].click
end

# Método de validação de alteração de senha.
def alteracao_senha
  puts "Fluxo de validação de alteração de senha."
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click 
  @driver.find_element(:id, "change-password-link").click
  # A partir daqui aparece o modal de alteração de senha.
  # Nesse caso, temos que informar a senha atual e informar nova senha.
  @driver.find_element(:css, "#change-password .modal-body #current_password").send_keys "inicial1234"
  @driver.find_element(:id, "new_password").send_keys "inicial12345"
  @driver.find_element(:id, "password_confirmation").send_keys "inicial12345"
  @driver.find_element(:css, "#change-password-form .action-buttons .btn").click
  sleep 05
  end 

def calcula_desconto
# Método de calculo de desconto do voucher. O valor por hora será e 12%.    
  @buscavalor = @driver.find_element(:xpath, "/html/body/div[3]/div[2]/section/div[2]/form/fieldset/div[2]/div/div[3]/p/strong").text
  @valor = @buscavalor.match( /(\d+,\d+)/ ).captures.first
  @valor = @valor.gsub(',', '.')
  @des = @valor.to_f*0.12
  @total = (@valor.to_f - @des).round(2)
  @total = @total.to_s
# Fim do cálculo.
end

# Método de validação de alteração de email com email válido.
def alteracao_email
  @email_compra_altera = Faker::Internet.email
  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_compra.massausuario
  @driver.get(@base_url)
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click      
  @driver.find_element(:id, "change-email-link").click
  @driver.find_element(:id, "new_email").send_keys @email_compra_altera
  @driver.find_element(:id, "confirm_email").send_keys @email_compra_altera
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "#change-email-form .btn").click
  sleep 10
  end

####### Inicio dos testes #########
### Método de Primeira Compra de Revistas Paga ###
def test_primeira_compra_revistas
  puts "Fluxo principal de compra de revistas com valor sem One Click Buy."
  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_compra.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos(dados)
end
### Fim do Método de Primeira Compra de Revistas ###

### Método de validação de dados no cadastro completo. ###
def test_valida_cadastro_completo
  puts "Fluxo de  validação de dados no cadastro completo."
  test_primeira_compra_revistas
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  assert(@driver.find_elements(:css, ".address-fields dd")[0].text == "04679-230")
  assert(@driver.find_elements(:css, ".address-fields dd")[1].text == "Eurico Leme Ramos")
  assert(@driver.find_elements(:css, ".address-fields dd")[2].text == "12345")
  assert(@driver.find_elements(:css, ".address-fields dd")[3].text == "Casa1")
  assert(@driver.find_elements(:css, ".address-fields dd")[4].text == "Vila Santana")
  assert(@driver.find_elements(:css, ".address-fields dd")[5].text == "Sao Paulo")
  assert(@driver.find_elements(:css, ".address-fields dd")[6].text == "São Paulo")
### Print da tela com o resultado esperado.    
    time = Time.new
    @driver.save_screenshot(File.dirname('/Users/Vendaval/Desktop/screenshots') + '/screenshots/' + @method_name + '_' + time.strftime('%Y%m%d_%H%M%S') + '.png')
end
### Fim do Método de validação de dados no cadastro completo. ####


### Método de validação de email de confirmação de compra.
def test_valida_email_confirmacao_compra
  puts "Fluxo de validação de confirmação de pedido através do email."
  test_primeira_compra_revistas
  nro_pedido = @driver.find_elements(:css, "p.order-info strong")[2].text
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_compra.email
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @testecampo = @driver.find_elements(:css, "ul.messages_list li.email .title")[0].text
  @driver.find_elements(:css, "ul.messages_list li.email .title")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
## Confirmo o número do pedido do cliente, de acordo com a tela de confirmação de pedido, comparando com o número do pedido que está no email.  
  sleep 05
  assert(@driver.find_element(:xpath, "/html/body/table/tbody/tr/td/div/p").text.include?(nro_pedido))
  assert(@driver.find_element(:xpath, "/html/body/table/tbody/tr/td/div/p").text.include?("Obrigado pelo seu pedido."))
end
### Fim do método de validação de email de confirmação de compra.

### Método de Primeira Compra de Revistas Paga virando One Click Buy###
def test_primeira_compra_revistas_ocb
  puts "Fluxo princiapl de compra de revistas com cliente One Click Buy."
  test_primeira_compra_revistas
### Aqui inicia a compra com o cliente já One Click Buy.
  @driver.find_element(:id, "save-card").click
  sleep 03
  @driver.find_element(:css, "#save-card-form .btn").click
  sleep 10
  assert(@driver.find_element(:css, ".flash.info.success").text == "Os dados do seu cartão de crédito foram salvos com sucesso")
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "button.btn").click
  #@driver.find_element(:css, "#confirm-order-modal-form button.btn").click -- Funcionalidade antiga do OCB
  sleep 10
end
### Fim do Método de Primeira Compra de Revistas ###

### Método de Validação de solicitação de senha após alteração de Senha com cliente OCB
def test_solicita_senha_apos_alteracao_ocb
  puts "Fluxo de validação de solicitação de senha após alterar senha com cliente ocb."  
  test_primeira_compra_revistas
  @driver.find_element(:id, "save-card").click
  sleep 03
  @driver.find_element(:css, "#save-card-form .btn").click
  sleep 10
  assert(@driver.find_element(:css, ".flash.info.success").text == "Os dados do seu cartão de crédito foram salvos com sucesso")
  alteracao_senha
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "#confirm-order-modal-form button.btn").click
  sleep 03
  assert(@driver.find_element(:css, "#order-confirmation-modal .modal-body .alert.alert-error").text == "A senha está incorreta.")
    ### Printa a tela pós senha inválida.
    time = Time.new
    @driver.save_screenshot(File.dirname('/Users/Vendaval/Desktop/screenshots') + '/screenshots/' + @method_name + '_' + time.strftime('%Y%m%d_%H%M%S') + '.png')
    ### Printa a tela pós senha inválida. 
  @driver.find_element(:id, "password").send_keys "inicial12345"
  @driver.find_element(:css, "#confirm-order-modal-form p.button-holder button.btn").click
  sleep 10
  assert(@driver.find_element(:css, "#order-success-modal.modal .modal-header h3").text == "O seu pedido foi finalizado com sucesso.")
  @driver.find_element(:css, "#order-success-modal.modal .modal-header button.close").click
end
### Fim do Método de Validação de solicitação de senha após alteração de Senha com cliente OCB

### Método de Primeira Compra de Revistas utilizando voucher de desconto.###
def test_primeira_compra_revistas_voucher
  puts "Fluxo principal de comra de revistas utilizando voucher de desconto."
  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_compra.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  @driver.find_element(:id, "user_address_postal_code").send_keys dados.cep
  sleep 05
  @driver.find_element(:id, "user_cpf").send_keys dados.cpf
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
  @driver.find_element(:id, "billing-details-submit").click 
### Agora eu calculo o voucher.
  calcula_desconto
  @driver.find_element(:id, "coupon_code").send_keys cadastro_generico_compra.cupom
  @driver.find_element(:css, ".voucher-form .btn").click
  sleep 10
### Aqui eu transformo o valor total em uma string para validar o teste.
  @validapreco = @driver.find_element(:css, "form#confirm-order-form.confirm-order-form fieldset.product-details-container div.left div.product-details div.price-info p.price-with-discount span.highlight-fade strong.price-placeholder").text
  @validapreco = @validapreco.match( /(\d+,\d+)/ ).captures.first
  @validapreco = @validapreco.to_s
  @validapreco = @validapreco.gsub(',', '.')
### Aqui eu faço uma comparação entre o valor calculado e o valor mostrado.
  if @validapreco == @total
  then
  puts "O valor do produto está correto."
  else
  puts "O teste está inválido."
  end
  sleep 05
  assert(@driver.find_element(:css, ".voucher-display div.voucher-display-message").text == "Após selecionar \"Efetuar Pagamento\" você não poderá remover o cupom")
  @driver.find_element(:id, "card_type_visa_online").click
  sleep 10
  @driver.find_element(:css, "#main-content .btn.btn-info").click
  @driver.switch_to.frame("global-collect-frame")
  @driver.find_element(:css, "#F1009").send_keys "4242424242424242"
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:css, "#btnSubmit").click
  sleep 20
end
### Fim do Método de Primeira Compra de Revistas com voucher ###

### Método de compra de Revistas quando eu seleciono uma revista.
def test_primeira_compra_sinopse_revistas
  puts "Fluxo principal de compra de revistas através da capa."
  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_compra.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  random_sinopse
  @driver.find_element(:css, ".purchase button.btn").click
  sleep 10
  pagamentos(dados)
end
### Fim do método de compra

### Método de Primeira Compra de Revistas Gratuita ###
def test_primeira_compra_revistas_gratuitas
  puts "Fluxo principal de compra de revistas gratuitas sem One Click Buy."
  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "resgatar-gratis")
  sleep 05
  assert(@driver.find_element(:css, "#order-success-modal div.modal-header h3").text == "O seu pedido foi finalizado com sucesso.")
  @driver.find_element(:css, "#order-success-modal div.modal-footer button.btn.btn-info").click
end
### Fim do Método de Primeira Compra de Revistas ###

### Método de validação de dados de cartão de crédito.
def test_valida_informacoes_ocb
  puts "Fluxo de validação de dados de cartão de crédito com cliente OCB."
  test_primeira_compra_revistas_ocb
  @driver.find_element(:css, "#order-success-modal.modal .modal-header button.close").click
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  sleep 10
  assert(@driver.find_elements(:css, "dl.credit-card-fields dt label")[1].text == "Número do cartão")
end
### Fim do método de validação de dados.

### Método de validação de remoção de cartão de crédito.
def test_valida_remocao_cartao
  puts "Fluxo de validação de remoção de cartão de crédito."
  test_primeira_compra_revistas_ocb
  @driver.find_element(:css, "#order-success-modal.modal .modal-header button.close").click
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click
  @driver.find_element(:css, ".card-fields-container .remove-card a").click
  sleep 05
  assert(@driver.find_element(:css, "#remove-card.modal div.modal-body p#remove-header").text == "Deseja excluir este cartão? Se você é membro do iba clube perderá acesso sem um cartão válido cadastrado para pagamento.")
  @driver.find_element(:css, "#remove-card.modal div.modal-body div.action-buttons a.btn").click
  assert(@driver.find_element(:css, "#notifications ul.flash li").text == "O cartão de crédito foi removido com sucesso")    
end
### Fim do método de validação de remoção de cartão.

### Método de validação de compra quando o cliente remove o cartão.
def test_compra_pos_remocao_cartao
  puts "**** Fluxo de validação de compra quando cliente remove o cartão de crédito. ****"
  test_valida_remocao_cartao
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "card_type_visa_online").click
  @driver.find_element(:css, "#main-content .btn.btn-info").click
  @driver.switch_to.frame("global-collect-frame")
  @driver.find_element(:id, "F1009").send_keys "4242424242424242"
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:id, "btnSubmit").click
  sleep 10
end
### Fim do método de validação de compra quando cliente remove o cartão.

### Método de validação de email de confirmação de compra pós alteracao de email
def test_valida_email_confirmacao_altera_email
  puts "Fluxo de validação de confirmação de email de compra após alteração de email."
  dados = cadastro_generico_compra.massausuario
  alteracao_email
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos(dados)
  ### Validar email de confirmacao de compra após cliente ter alterado a senha.
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys @email_compra_altera 
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @testecampo = @driver.find_elements(:css, "ul.messages_list li.email .title")[0].text
  @driver.find_elements(:css, "ul.messages_list li.email .title")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  assert(@driver.find_element(:xpath, "/html/body/table/tbody/tr/td/div/p").text.include?("Obrigado pelo seu pedido."))
end
### Fim método de validação de email de confirmação de compra pós alteracao de email





### Método de Assinaturas ###
###def test_assine_revistas
###  puts "Fluxo principal de assinatura de revistas com valor sem One Click Buy."
###  cadastro_generico_compra.cadastro_cliente(@driver, @base_url)
###  dados = cadastro_generico_compra.massausuario
###  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
###  @driver.find_element(:id, "show_more_link").click
###  @driver.find_element(:id, "show_more_link").click
###  sleep 10
###  scripts_initialize.random_assine(@driver, "assine")
###  sleep 10
###  @driver.find_elements(:css, ".buy .create-order-form button.btn")[0].click
###  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
###  @driver.find_element(:id, "user_address_postal_code").send_keys "04679-230"
###  sleep 05
###  @driver.find_element(:id, "user_cpf").send_keys dados.cpf
###  @driver.find_element(:id, "user_address_street_number").send_keys "11"
###  @driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
###  @driver.find_element(:id, "user_dob_d").send_keys "01"
###  @driver.find_element(:id, "user_dob_m").send_keys "01"
###  @driver.find_element(:id, "user_dob_y").send_keys "1980"
###  @driver.find_element(:id, "user_gender_m").click
###  @driver.find_element(:css, "button.btn.btn-info").click
###  @driver.find_element(:css, ".terms-and-conditions label.checkbox input").click
###  sleep 10
###  @driver.find_element(:css, "button.btn.btn-info").click
###  @driver.find_element(:css, "#estado.combo").click
###  @driver.find_element
###  sleep 10
###end
### Fim do Método de Assinaturas ###


end
