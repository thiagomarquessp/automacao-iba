# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects
### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.
### Mais um para se basear em melhores práticas: http://ithaca.arpinum.org/2010/07/29/ruby-dynamic-includes.html
### Para executar algum método específico, utilizar o seguinte comando: ruby -I"lib:test" ~/Desktop/Automacaoo/loja/webstore.rb -n test_cadastro_cliente
###  sales@froglogic.com.

require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require "faker_credit_card"
require '../classes/scripts'
require '../classes/massausuario'
require '../classes/pagamentos'

class NovaCompraRevista < Test::Unit::TestCase
	def setup
		@driver = Selenium::WebDriver.for :firefox
    	@base_url = "http://store-qa.ibacloud.com.br/"
    	@accept_next_alert = true
    	@driver.manage.timeouts.implicit_wait = 60
    	@verification_errors = []
	end

  def teardown
    @driver.quit
    assert_equal [], @verification_errors
  end
  
  def element_present?(how, what)
    @driver.find_element(how, what)
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end
  
  def alert_present?()
    @driver.switch_to.alert
    true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  end
  
  def verify(&blk)
    yield
  rescue Test::Unit::AssertionFailedError => ex
    @verification_errors << ex
  end
  
  def close_alert_and_get_its_text(how, what)
    alert = @driver.switch_to().alert()
    alert_text = alert.text
    if (@accept_next_alert) then
      alert.accept()
    else
      alert.dismiss()
    end
    alert_text
  ensure
    @accept_next_alert = true
  end

  def  scripts_initialize
    scripts = ScriptsJs.new
  end

  def  massausuario
      @massa ||= MassadeDados.new
  end

  def usuario
    usuario_existente = Usuarios.new
  end

  def validahistorico
    @history ||= Historicos.new
  end
# Método de login com usuário existente
def user_existente
 @driver.get "https://store-qa.ibacloud.com.br/"
 @driver.find_element(:css, "button.close").click
 @driver.find_element(:id, "header-login-link").click
 @driver.find_element(:id, "login_email").send_keys "user_exist@iba.com"
 @driver.find_element(:id, "login_password").send_keys "inicial1234"
 @driver.find_element(:id, "submit-signin").click
end
### Fim do Método de Usuário existente.

### Início dos testes
def test_nova_compra_jornais
  puts "Fluxo de validação de nova compra de jornais."
  user_existente
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[2].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  @driver.find_element(:id, "card_type_visa_online").click
  @driver.find_element(:css, "#main-content .btn.btn-info").click
  @driver.switch_to.frame("global-collect-frame")
  @credit_card = Faker::CreditCard::Visa.number(length: 16)
  @driver.find_element(:id, "F1009").send_keys @credit_card
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:id, "btnSubmit").click
  sleep 10
end

### Fim do método de Novas Compras ###
  

end
