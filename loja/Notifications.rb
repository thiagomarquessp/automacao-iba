# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API

### Pensar em melhores práticas sempre
require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require 'faker'
require "faker_credit_card"
require '../classes/scripts'
require '../classes/massausuario'
require '../classes/pagamentos'

class AlteraCadastro < Test::Unit::TestCase

  

### Configurações e chamadas de outros métodos e classes de outros arquivos 
  def setup
    @driver = Selenium::WebDriver.for :firefox
    @base_url = "https://store-qa.ibacloud.com.br/"
    @accept_next_alert = true
    @driver.manage.timeouts.implicit_wait = 60
    @verification_errors = []
  end

  def teardown
    @driver.quit
    assert_equal [], @verification_errors
  end
  
  def element_present?(how, what)
    @driver.find_element(how, what)
    true
    rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end
  
  def alert_present?()
    @driver.switch_to.alert
    true
    rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  end
  
  def verify(&blk)
    yield
    rescue Test::Unit::AssertionFailedError => ex
    @verification_errors << ex
  end
  
  def close_alert_and_get_its_text(how, what)
    alert = @driver.switch_to().alert()
    alert_text = alert.text
      if (@accept_next_alert) then
      alert.accept()
      else
      alert.dismiss()
      end
      alert_text
      ensure
      @accept_next_alert = true
  end

  def  scripts_initialize
    scripts = ScriptsJs.new
  end

  def  massausuario
    @massa ||= MassadeDados.new
  end

  def usuario
    usuario_existente = Usuarios.new
  end

  def cadastro_generico_notificacoes
    @cad_generico ||= CadastroGenerico.new
  end

  def pagamentos_notification(dados)
    @pagamento ||= Pagamentos.new(@driver, dados)
  end

### Método de Primeira Compra de Revistas Paga ###
def primeira_compra_revistas
  puts "Fluxo principal de compra de revistas com valor sem One Click Buy."
  cadastro_generico_notificacoes.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_notificacoes.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  assert(@driver.find_element(:css, ".secure-information").text == "Seus dados serão transmitidos com segurança, via SSL.")
  pagamentos_notification(dados)
end
### Fim do Método de Primeira Compra de Revistas ###


### Aqui se inicia os testes de Cadastro

# Método de validação de Notificação de Cadastro de usuário.
def test_cadastro_cliente
  puts "Fluxo de validação de Notificação de cadastro de Cliente (Email de Boas Vindas)."
  cadastro_generico_notificacoes.cadastro_cliente(@driver, @base_url)
  sleep 05
  ### A partir daqui irei validar o email de boas vindas do cliente cadastrado.
  @driver.get "https://mailtrap.io"
  sleep 05
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_notificacoes.email
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @driver.find_elements(:css, "ul.messages_list li.email")[1].click
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @testecampo = @driver.find_elements(:css, "ul.messages_list li.email .title")[0].text
  if @testecampo == "Bem-vindo ao iba"
    @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
    assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text ==  "Bem vindo ao iba! Para sua segurança, enviamos um e-mail para verificação do seu cadastro.
Para confirmar o seu cadastro, clique no link abaixo ou copie e cole no seu navegador:")
  @driver.find_element(:xpath, "/html/body/div[2]/div/p[3]/a").click
    else if @testecampo == "iba - Seu cupom de desconto"
     @driver.find_elements(:css, "ul.messages_list li.email .title")[1].click 
    @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
    assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text == "Bem vindo ao iba! Para sua segurança, enviamos um e-mail para verificação do seu cadastro.
Para confirmar o seu cadastro, clique no link abaixo ou copie e cole no seu navegador:") 
    @driver.find_element(:xpath, "/html/body/div[2]/div/p[3]/a").click
    end
  end
end    
### Fim do Método de validação de Notificação de Cadastro de usuário ###

### Método de validação de email de contratação com voucher.
def test_cadastro_cliente_notf_voucher
  puts "Fluxo de validação de Notificação de cadastro de Cliente (Email de recebimento de voucher)."
  cadastro_generico_notificacoes.cadastro_cliente(@driver, @base_url)
  sleep 05
  ### A partir daqui irei validar o email de boas vindas do cliente cadastrado.
  @driver.get "https://mailtrap.io"
  sleep 05
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_notificacoes.email
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @driver.find_elements(:css, "ul.messages_list li.email")[1].click
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @testecampo = @driver.find_elements(:css, "ul.messages_list li.email .title")[0].text
  if @testecampo == "iba - Seu cupom de desconto"
    @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
    assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text ==  "Você acaba de ganhar 12% para adquirir uma publicação digital no site do iba.")
  assert(@driver.find_element(:xpath, "/html/body/div[2]/div/div").text == cadastro_generico_notificacoes.cupom)
    else if @testecampo == "Bem-vindo ao iba"
     @driver.find_elements(:css, "ul.messages_list li.email .title")[1].click 
    @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
    assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text == "Você acaba de ganhar 12% para adquirir uma publicação digital no site do iba.") 
   assert(@driver.find_element(:xpath, "/html/body/div[2]/div/div").text == cadastro_generico_notificacoes.cupom)
    end
  end
end    
### Fim do Método de validação de Notificação de Cadastro de usuário ###

# Método de validação de notificação de alteração de senha.
def test_valida_alteracao_senha_not
    puts "Fluxo de validação de alteração de senha."
    cadastro_generico_notificacoes.cadastro_cliente(@driver, @base_url)
    @driver.find_element(:id, "user-name-link").click
    @driver.find_elements(:css, ".user-profile a")[1].click
    @driver.find_element(:css, "#profile-container .btn").click 
    @driver.find_element(:id, "change-password-link").click
    # A partir daqui aparece o modal de alteração de senha.
    # Nesse caso, temos que informar a senha atual e informar nova senha.
    @driver.find_element(:css, "#change-password .modal-body #current_password").send_keys "inicial1234"
    @driver.find_element(:id, "new_password").send_keys "inicial12345"
    @driver.find_element(:id, "password_confirmation").send_keys "inicial12345"
    @driver.find_element(:css, "#change-password-form .action-buttons .btn").click
    sleep 05
    assert(@driver.find_element(:css, ".container .success").text == "Senha alterada com sucesso. Um e-mail será enviado com os detalhes para verificação.")
    assert(@driver.find_element(:id, "user-name-link").text == cadastro_generico_notificacoes.nome)
## Aqui eu valido o email de alteracao de senha
    @driver.get "https://mailtrap.io"
    sleep 10
    @driver.find_element(:css, "a.button.transparent").click
    @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
    @driver.find_element(:id, "user_password").send_keys "abril@123"
    @driver.find_element(:css, "input.button.mrs").click
### A Caixa de entrada a ser utilizada é a de QA
    @driver.find_elements(:css, "span.inbox_name")[1].click
    sleep 05
    @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_notificacoes.email
    sleep 05
    @driver.find_elements(:css, "ul.messages_list li.email div.row")[0].click
    @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))  
    assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text == "Sua senha foi alterada com sucesso no iba, a maior loja de conteúdo digital do Brasil.")
end 
# Método de de validação de notificação de alteração de senha.

### Método de validação de notificação de alteração de email
def test_notificacao_alteracao_email
  puts "Validar se usuario consegue cancelar a alteracao de email após ativar a alteracao de email."
  cadastro_generico_notificacoes.cadastro_cliente(@driver, @base_url)
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click      
  @driver.find_element(:id, "change-email-link").click
  @altera_email_notifica = Faker::Internet.email
  @driver.find_element(:id, "new_email").send_keys @altera_email_notifica
  @driver.find_element(:id, "confirm_email").send_keys @altera_email_notifica
  @driver.find_element(:id, "password").send_keys "inicial1234"
  @driver.find_element(:css, "#change-email-form .btn").click
 ## Aqui eu valido o email de alteracao de senha
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 05
  @driver.find_element(:css, "input.quick_filter").send_keys @altera_email_notifica
  sleep 05
  @driver.find_elements(:css, "ul.messages_list li.email div.row")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text == "Foi solicitada a troca do seu e-mail no iba, a maior loja de conteúdo digital do Brasil.
Para confirmar o seu novo e-mail, clique no link abaixo ou copie e cole no seu navegador:")
  ## Agora vou verificar email de alteração no email antigo.
  @driver.switch_to.default_content
  @driver.find_element(:css, "input.quick_filter").clear()
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_notificacoes.email
  sleep 05
  @driver.find_elements(:css, "ul.messages_list li.email div.row")[0].click
  @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
  assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text == "Seu e-mail de acesso ao iba foi alterado.
Caso você não tenha feito esta solicitação, clique aqui para desfazer a mudança.")
end
### Fim do método de validação de notificação de alteração de email

### Método de validação de Notificação de compra.
def test_notificacao_compra
  puts "Fluxo principal de notificação de compra."
  cadastro_generico_notificacoes.cadastro_cliente(@driver, @base_url)
  dados = cadastro_generico_notificacoes.massausuario
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "comprar")
  sleep 10
  pagamentos_notification(dados)
  nro_pedido = @driver.find_elements(:css, "p.order-info strong")[2].text
## Aqui eu valido o email de confirmacao de compra.
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_notificacoes.email
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @testecampo = @driver.find_elements(:css, "ul.messages_list li.email .title")[0].text
  @driver.find_elements(:css, "ul.messages_list li.email .title")[0].click
  @driver.switch_to.frame(@driver.find_element(:xpath, "//iframe"))
## Confirmo o número do pedido do cliente, de acordo com a tela de confirmação de pedido, comparando com o número do pedido que está no email.  
  sleep 05
  assert(@driver.find_element(:xpath, "/html/body/table/tbody/tr/td/div/p").text.include?(nro_pedido))
  assert(@driver.find_element(:xpath, "/html/body/table/tbody/tr/td/div/p").text.include?("Obrigado pelo seu pedido."))
end
### Fim do Método de Compra ###

### Método de validação de notificação de compra gratuita ###
def test_notificacao_compra_gratuita
  puts "Fluxo de validação de notificação de compra gratuita."
  cadastro_generico_notificacoes.cadastro_cliente(@driver, @base_url)
  @driver.find_elements(:css, "#header.container nav.product-links a.inactive")[0].click
  @driver.find_element(:id, "show_more_link").click
  @driver.find_element(:id, "show_more_link").click
  scripts_initialize.select_random_product_for(@driver, "resgatar-gratis")
  sleep 05
  nro_pedido_gratis = @driver.find_elements(:css, "p.order-info strong")[2].text
  puts "O número do pedido é: " + nro_pedido_gratis
  @driver.find_element(:css, "#order-success-modal div.modal-footer button.btn.btn-info").click
## Aqui eu valido o email de confirmacao de compra.
  @driver.get "https://mailtrap.io"
  sleep 10
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys cadastro_generico_notificacoes.email
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @testecampo = @driver.find_elements(:css, "ul.messages_list li.email .title")[0].text
  @driver.find_elements(:css, "ul.messages_list li.email .title")[0].click
  @driver.switch_to.frame(@driver.find_element(:xpath, "//iframe"))
## Confirmo o número do pedido do cliente, de acordo com a tela de confirmação de pedido, comparando com o número do pedido que está no email.  
  sleep 05
  assert(@driver.find_element(:xpath, "/html/body/table/tbody/tr/td/div/p").text.include?(nro_pedido_gratis))
  assert(@driver.find_element(:xpath, "/html/body/table/tbody/tr/td/div/p").text.include?("Obrigado pelo seu pedido."))  
end
### Fim do Método de validação de notificação de compra gratuita ###

### Método de Primeira Compra de Revistas Paga virando One Click Buy###
def test_notificacao_alteracao_cartao_credito
  puts "Fluxo princiapl de compra de revistas com cliente One Click Buy."
  primeira_compra_revistas
### Aqui inicia a compra com o cliente já One Click Buy.
  @driver.find_element(:id, "save-card").click
  sleep 03
  @driver.find_element(:css, "#save-card-form .btn").click
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click  
  @driver.find_element(:css, ".card-fields-container .btn").click
  sleep 05
  @novocartao = Faker::CreditCard::Visa.number(length: 16)
  @driver.find_element(:id, "card_type_visa_online").click
  sleep 10
  @driver.switch_to.frame("global-collect-frame")
  @driver.find_element(:id, "F1009").send_keys @novocartao
  @driver.find_element(:id, "F1136").send_keys "123"
  @driver.find_element(:id, "F1010_MM").send_keys "01"
  @driver.find_element(:id, "F1010_YY").send_keys "20"
  @driver.find_element(:css, "#btnSubmit").click
  sleep 10
  @driver.switch_to.alert.accept
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  @driver.find_element(:css, "#profile-container .btn").click
end
### Fim do Método de Primeira Compra de Revistas ###

end


