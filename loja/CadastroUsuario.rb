# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects
### http://flavio.castelli.name/2010/05/28/rails_execute_single_test/ -- para execução de metodos e derivados.
### Mais um para se basear em melhores práticas: http://ithaca.arpinum.org/2010/07/29/ruby-dynamic-includes.html
### Para executar algum método específico, utilizar o seguinte comando: ruby -I"lib:test" ~/Desktop/Automacaoo/loja/webstore.rb -n cadastro_cliente
###  sales@froglogic.com.

require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require '../classes/scripts'
require '../classes/massausuario'


class CadastroCompleto < Test::Unit::TestCase

  attr_reader :cupom

### Configurações e chamadas de outros métodos e classes de outros arquivos	
def setup
		  @driver = Selenium::WebDriver.for :firefox
    	@base_url = "https://store-qa.ibacloud.com.br/"
    	@accept_next_alert = true
    	@driver.manage.timeouts.implicit_wait = 60
    	@verification_errors = []
end

def teardown
  @driver.quit
  assert_equal [], @verification_errors
end
  
def element_present?(how, what)
  @driver.find_element(how, what)
  true
  rescue Selenium::WebDriver::Error::NoSuchElementError
  false
end
  
def alert_present?()
  @driver.switch_to.alert
  true
  rescue Selenium::WebDriver::Error::NoAlertPresentError
  false
end
  
def verify(&blk)
  yield
  rescue Test::Unit::AssertionFailedError => ex
  @verification_errors << ex
end
  
def close_alert_and_get_its_text(how, what)
  alert = @driver.switch_to().alert()
  alert_text = alert.text
    if (@accept_next_alert) then
    alert.accept()
    else
    alert.dismiss()
    end
    alert_text
  	ensure
    @accept_next_alert = true
end

def  scripts_initialize
  scripts = ScriptsJs.new
end

def usuario
 	usuario_existente = Usuarios.new
end

def cadastro_generico
  @cad_generico ||= CadastroGenerico.new
end

def  massausuario
  @massa ||= MassadeDados.new
end

### Aqui se inicia os testes de Cadastro
### Contexto: Com o objetivo de aumentar a cobertura de testes do Fluxo de Cadastro de Usuário, 
### assim como, automatizar o mesmo, devemos iniciar a criação dos scripts dos cenários de testes já definidos.

# Método de Cadastro Completo - Fluxo Principal #
def cadastro_cliente
  cadastro_generico.cadastro_cliente(@driver, @base_url)
  #@driver.find_element(:css, "html.js body.users-controller div.container div#main-content div#login-sign-up.page div#coupon-modal.modal div.modal-footer button.btn").click
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  assert(@driver.find_element(:css, ".user-info-fields dd").text == cadastro_generico.nome)
  assert(@driver.find_elements(:css, "#profile-container .user-info-fields dd")[1].text == cadastro_generico.email.downcase)
end
### Fim do Método de Cadastro de Cliente ###

# Método de validação de Email com formato inválido
def test_valida_email_invalido
  puts 'Validação de email com formato inválido.'
  massausuario.fake_generator(@driver)
	@driver.get(@base_url)
	@driver.find_element(:css, "button.close").click
	@driver.find_element(:id, "header-sign-up-link").click
	@driver.find_element(:id, "sign_up_email").send_keys "abc123@"
	@driver.find_element(:id, "sign_up_email_confirmation").send_keys "123abc@"
	@driver.find_element(:id, "sign_up_terms_and_conditions").click
  @driver.find_element(:css, "#sign-up-form .btn.info.btn-info").click
	@verifica_email = @driver.find_elements(:css, "label.error")[1].text
	@confirma_email = @driver.find_elements(:css, "label.error")[2].text
	assert(@driver.find_elements(:css, "label.error")[1].text == "Por favor, informe um e-mail válido.")
	assert(@driver.find_elements(:css, "label.error")[2].text == "Por favor, informe um e-mail válido.")
end
# Fim do método de validação de Email com formato inválido

# Método de validação de Email existente
def test_valida_email_existente
	puts "Validação de cadastro com email já existente."
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-sign-up-link").click
  @driver.find_element(:id, "sign_up_email").send_keys "a@z.com.br"
  @driver.find_element(:id, "sign_up_email_confirmation").send_keys "a@z.com.br"
 	@driver.find_element(:id, "sign_up_name").send_keys massausuario.nome
 	@driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
	@driver.find_element(:id, "sign_up_password_confirmation").send_keys massausuario.senha_padrao
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  @driver.find_element(:css, "#sign-up-form .btn.info.btn-info").click
  @validaemail_existente = @driver.find_element(:css, "div.social-link-account-text h4").text
  puts @validaemail_existente
  assert(@driver.find_element(:css, "div.social-link-account-text h4").text == "Você já possui uma conta no iba com esse e-mail, para conectar sua conta do Facebook ao iba, basta confirmar sua conta abaixo.")
end
# Fim do método de validação de Email existente

# Método de validação de Email existente
def test_valida_emails_diferentes
  puts "Testes de validação de cadastro com emails diferentes"
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-sign-up-link").click
  @driver.find_element(:id, "sign_up_email").send_keys "q@z.com.br"
  @driver.find_element(:id, "sign_up_email_confirmation").send_keys "a@z.com.br"
  @driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
	@valida_email_diferentes = @driver.find_element(:css, "label.error").text
	assert(@driver.find_element(:css, "label.error").text == "Os e-mails não coincidem. Por favor, digite novamente.")
end
# Fim do Método de validação de Email existente

# Método de Validação de Senha Inválida
def test_valida_senha_invalida
  puts "Teste de Validação de Senha inválida"
  massausuario.fake_generator(@driver)
 	@driver.get(@base_url)
	@driver.find_element(:css, "button.close").click
 	@driver.find_element(:id, "header-sign-up-link").click
  @driver.find_element(:id, "sign_up_password").send_keys "asdfghjk"
  @driver.find_element(:id, "sign_up_password_confirmation").send_keys "asdfghjk"
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  @valida_senha = @driver.find_elements(:css, "label.error")[0].text
  @valida_confirma_senha = @driver.find_elements(:css, "label.error")[1].text
  assert(@driver.find_elements(:css, "label.error")[0].text == "A senha deve conter pelo menos 8 caracteres, incluindo letras e números.")
  assert(@driver.find_elements(:css, "label.error")[1].text == "A senha deve conter pelo menos 8 caracteres, incluindo letras e números.")
end
# Fim do Método de Validação de Senha Inválida

# Método de Validação de Senhas Diferentes
def test_valida_senhas_diferentes
  puts "Teste de Validação de Senhas Diferentes no cadastro do cliente"
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-sign-up-link").click
  @driver.find_element(:id, "sign_up_password").send_keys "inicial1234"
  @driver.find_element(:id, "sign_up_password_confirmation").send_keys "inicial12345"
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  sleep 10
  @valida_senha_diferente = @driver.find_element(:css, "label.error").text
  puts @valida_senha_diferente
  assert(@driver.find_element(:css, "label.error").text == "As senhas não coincidem. Por favor, digite novamente.")
end
# Fim do Método de Validação de Senhas Diferentes

# Método de Validação de Foco em campo que não foi preenchido
def test_valida_foco
  puts "Teste de Validação de Foco, quando o campo não é preenchido no cadastro. Nesse caso, será o campo Nome"
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-sign-up-link").click
  @driver.find_element(:id, "sign_up_email").send_keys massausuario.email
  @driver.find_element(:id, "sign_up_email_confirmation").send_keys massausuario.email
  @driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
  @driver.find_element(:id, "sign_up_password_confirmation").send_keys massausuario.senha_padrao
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  @driver.find_element(:css, "#sign-up-form .btn.info.btn-info").click
  sleep 10    
  @campo_foco = @driver.find_element(:css, "label.error").text
  assert(@driver.find_element(:css, "label.error").text == "Por favor, informe o seu nome para criar um cadastro.")
end
# Fim do Método de Validação de Foco em campo que não foi preenchido

# Método de Validação de login quando o cliente esquece a senha (Solicitação de nova senha)
def test_esqueci_minha_senha
  puts "Cenário de teste de login quando o cliente esquece a senha."
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "forgot-password-link").click
  @driver.find_element(:id, "forgot-password-email").send_keys "qa130201@iba.com"
  @driver.find_element(:css, "#forgot-password input.btn").click
  sleep 10
end
# Fim do Método de Validação de login quando o cliente esquece a senha (Solicitação de nova senha)

# Método de validação de mensagem que informa sobre o iba clube.
def test_valida_mensagem_ibaclube
  puts "Fluxo de validação de mensagem que informa sobre iba clube."
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:id, "login_email").send_keys "joo@xavier.biz"
  @driver.find_element(:id, "login_password").send_keys "inicial1234"
  @driver.find_element(:id, "submit-signin").click
  sleep 05
  @driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  assert(@driver.find_element(:css, "#side-nav.user ul li.membership a").text == "iba clube")
  @driver.find_element(:css, "#side-nav.user ul li.membership a").click
  sleep 05
  assert(@driver.find_element(:css, "#main-content .membership-invitation-lead").text == "Você não faz parte do iba clube!")
end
# Método de validação de mensagem que informa sobre o iba clube.


def test_cadastro_atraves_login
  puts "FLuxo de  cadastro através do login de usuário, clicando em não tenho cadastro."
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click
  @driver.find_element(:id, "header-login-link").click
  @driver.find_element(:link_text, "Faça o seu cadastro").click
  @driver.find_element(:id, "sign_up_email").send_keys massausuario.email
  @driver.find_element(:id, "sign_up_email_confirmation").send_keys massausuario.email
  @driver.find_element(:id, "sign_up_name").send_keys massausuario.nome
  @driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
  @driver.find_element(:id, "sign_up_password_confirmation").send_keys massausuario.senha_padrao
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  @driver.find_element(:css, "#sign-up-form .btn.info.btn-info").click
  sleep 10
  @driver.find_element(:css, "#coupon-modal .btn").click
  sleep 10
  ### A partir daqui irei validar o email de boas vindas do cliente cadastrado.
  @driver.get "https://mailtrap.io"
  sleep 05
  @driver.find_element(:css, "a.button.transparent").click
  @driver.find_element(:id, "user_email").send_keys "core@iba.com.br"
  @driver.find_element(:id, "user_password").send_keys "abril@123"
  @driver.find_element(:css, "input.button.mrs").click
  ### A Caixa de entrada a ser utilizada é a de QA
  @driver.find_elements(:css, "span.inbox_name")[1].click
  sleep 10
  @driver.find_element(:css, "input.quick_filter").send_keys massausuario.email
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @driver.find_elements(:css, "ul.messages_list li.email")[1].click
  @driver.find_elements(:css, "ul.messages_list li.email")[0].click
  @testecampo = @driver.find_elements(:css, "ul.messages_list li.email .title")[0].text
  if @testecampo == "Bem-vindo ao iba"
    @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
    assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text ==  "Bem vindo ao iba! Para sua segurança, enviamos um e-mail para verificação do seu cadastro.
Para confirmar o seu cadastro, clique no link abaixo ou copie e cole no seu navegador:")
  @driver.find_element(:xpath, "/html/body/div[2]/div/p[3]/a").click
    else if @testecampo == "iba - Seu cupom de desconto"
     @driver.find_elements(:css, "ul.messages_list li.email .title")[1].click 
    @driver.switch_to.frame (@driver.find_element(:xpath, "//iframe"))
    assert(@driver.find_element(:xpath, "/html/body/div[2]/div/p[2]").text == "Bem vindo ao iba! Para sua segurança, enviamos um e-mail para verificação do seu cadastro.
Para confirmar o seu cadastro, clique no link abaixo ou copie e cole no seu navegador:") 
    @driver.find_element(:xpath, "/html/body/div[2]/div/p[3]/a").click
    end
  end
end

### Método que valida o cadastro no descubra.
def test_cadastro_descubra
  puts "Fluxo de cadastro através do descubra."
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click 
  @driver.find_element(:link_text, "descubra o iba").click
  sleep 05
  @driver.find_element(:css, "#try-it span.ebooks").click
  sleep 05
  @driver.find_elements(:css, "#ebooks-showcase .publication-products label.product-label")[0].click
  @driver.find_elements(:css, "#ebooks-showcase .publication-products label.product-label")[1].click
  @driver.find_elements(:css, "#ebooks-showcase .publication-products label.product-label")[2].click
  @driver.find_elements(:css, "#ebooks-showcase .publication-products label.product-label")[3].click
  @driver.find_elements(:css, "#ebooks-showcase .publication-products label.product-label")[4].click
  sleep 05
  @driver.find_element(:css, "#ebooks-showcase .try-button-medium").click
  sleep 05
  @driver.find_element(:id, "sign_up_email").send_keys massausuario.email
  @driver.find_element(:id, "sign_up_email_confirmation").send_keys massausuario.email
  @driver.find_element(:id, "sign_up_name").send_keys massausuario.nome
  @driver.find_element(:id, "sign_up_password").send_keys "inicial1234"
  @driver.find_element(:id, "sign_up_password_confirmation").send_keys "inicial1234"
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  @driver.find_element(:css, "#sign-up-form .btn.info.btn-info").click
  sleep 10
  @cupom_descubra = @driver.find_element(:css, ".coupon-code-container").text
  puts "Esse é cupom gerado pelo seu cadastro: " + @cupom_descubra
  @driver.find_element(:css, "#coupon-modal .btn").click
  sleep 05
  assert(@driver.find_elements(:css, "#ebooks-showcase-success .success-text p")[0].text == "Os títulos que acabou de escolher estão esperando por você no iba e-books.")
  assert(@driver.find_elements(:css, "#ebooks-showcase-success .download-apps ul .app a.btn .text")[0].text == "Baixe o iba e-books para iPad®")
  assert(@driver.find_elements(:css, "#ebooks-showcase-success .download-apps ul .app a.btn .text")[1].text == "Baixe o iba e-books para tablets Android")
  assert(@driver.find_elements(:css, "#ebooks-showcase-success .download-apps ul .app a.btn .text")[2].text == "Baixe o iba e-books para Windows PC")
  assert(@driver.find_elements(:css, "#ebooks-showcase-success .download-apps ul .app a.btn .text")[3].text == "Baixe o iba e-books para iPhone®")  
  assert(@driver.find_elements(:css, "#ebooks-showcase-success .download-apps ul .app a.btn .text")[4].text == "Baixe o iba e-books para smartphone Android")
  assert(@driver.find_elements(:css, "#ebooks-showcase-success .download-apps ul .app a.btn .text")[5].text == "Baixe o iba e-books para Mac OS")
  assert(@driver.find_element(:css, "#ebooks-showcase-success.order-created h2 span").text == massausuario.nome)
end
### Fim do Método que valida o cadastro no descubra.

def test_botao_experimentar
  puts "Validar ação do botão experimentar."
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)
  @driver.find_element(:css, "button.close").click 
  @driver.find_element(:link_text, "descubra o iba").click
  sleep 05
  @driver.find_element(:link_text, "Experimente grátis!").click
  sleep 02
  assert(@driver.find_element(:css, ".magazines").text == "revistas")
  assert(@driver.find_element(:css, ".ebooks").text == "e-books")
  assert(@driver.find_element(:css, ".newspapers").text == "jornais")
end



end
