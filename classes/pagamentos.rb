# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#

require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require '../classes/massausuario'

class Pagamentos

def connection_refused
 	display = ENV['BUILD_NUMBER'] || "99"
 	@headless = Headless.new(:display => display)
 	@headless.start
end

def initialize(driver, massa_dados)
  realizar_primeiro_pgto(driver, massa_dados)
end

def realizar_primeiro_pgto(driver, dados)
 driver.find_element(:id, "user_address_postal_code").send_keys dados.cep
 sleep 05
 driver.find_element(:id, "user_cpf").send_keys dados.cpf
 driver.find_element(:id, "user_address_street_number").send_keys "12345"
 driver.find_element(:id, "user_address_street_line2").send_keys "Casa1"
 driver.find_element(:id, "billing-details-submit").click  
 driver.find_element(:id, "card_type_visa_online").click
 sleep 10
 driver.find_element(:css, "#main-content .btn.btn-info").click
 driver.switch_to.frame("global-collect-frame")
 driver.find_element(:css, "#F1009").send_keys dados.visa
 driver.find_element(:id, "F1136").send_keys "123"
 driver.find_element(:id, "F1010_MM").send_keys "01"
 driver.find_element(:id, "F1010_YY").send_keys "20"
 driver.find_element(:css, "#btnSubmit").click
 sleep 20
  #assert(@driver.find_element(:css, "#order-success p.successful-purchase").text == "O seu pedido foi finalizado com sucesso.")      
end
end