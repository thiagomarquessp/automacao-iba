# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#
require "json"
require "selenium-webdriver"
gem "test-unit"
require "test/unit"
require "../classes/massausuario"

### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects

class ScriptsJs

def initialize
  @clicados = []
end
	
def connection_refused
	display = ENV['BUILD_NUMBER'] || "99"
 	@headless = Headless.new(:display => display)
 	@headless.start
end

# Logo abaixo temos o metodo que faz o random. Foi colocado um parâmetro a mais para validar itens gratuitos ou itens pagos. 
# Os parâmetros são trabalhados em "comprar" para itens pagos e "resgatar-gratis" para itens gratuitos.
# Olhei no https://gist.github.com/huangzhichong/3284966
# OBS: Deverá tomar cuidado como XPATH, pois se algo é mudado na tela, temos que dar manutenção, senão vai quebrar toda a automação. 
def select_random_product_for(driver, tipoitem)
  botoes = driver.find_elements(:xpath, "//button[@data-ga-action='#{tipoitem}']").size
  busca = Random.rand(botoes)
  driver.find_elements(:xpath, "//button[@data-ga-action='#{tipoitem}']")[busca].click
end

### Método de Random de compra no detalhe do produto.
def random_detalhe(driver, tipoitem)
  detalhe = driver.find_elements(:xpath, "//a[@data-ga-action='#{tipoitem}']").size
  busca = Random.rand(detalhe)
  driver.find_elements(:xpath, "//a[@data-ga-action='#{tipoitem}']")[busca].click
end
### Fim do Método de Random de compra no detalhe do produto.

### Método de Random de compra de um ebook de dentro detalhe do produto.
def random_detalhe_compra(driver, tipoitem)
  detalhe_compra = driver.find_elements(:xpath, "//button[@data-ga-action='#{tipoitem}']").size
  busca = Random.rand(detalhe_compra)
  driver.find_elements(:xpath, "//button[@data-ga-action='#{tipoitem}']")[busca].click
end
### Fim do Método de Random de compra de um ebook de dentro detalhe do produto.

### Método de Random para assinaturas
def random_assine(driver, tipoitem)
  assine = driver.find_elements(:partial_link_text, "assine").size
  busca = Random.rand(assine)
  driver.find_elements(:partial_link_text, "assine")[busca].click
end
### Fim do Método de Random para assinaturas

def random_ibaclube_revistas(driver)
  li_random_pos = driver.execute_script("return Math.floor(Math.random()*17)+1")
  while @clicados.include? li_random_pos
  li_random_pos = driver.execute_script("return Math.floor(Math.random()*17)+1")
  end
  @clicados << li_random_pos
  driver.find_element(:xpath, "/html/body/section/div/form/ul/li[#{li_random_pos}]/label/div/img").click
end                                                   
### Fim do Método de Random. ###
end
### Classe de Pagamentos

class Historicos

  attr_reader :nm_rev_comprada, :historicoeb, :hist_nropedido, :nm_prod_comprado, :hist_nm_produto, :rev_comprada, :prod_comprado

def verify(&blk)
  yield
  rescue Test::Unit::AssertionFailedError => ex
  @verification_errors << ex
end

# Histórico de Primeira Compra de Revistas
def primeiracompra(driver)
  @nm_rev_comprada = driver.find_element(:xpath, "/html/body/div[3]/div[2]/div/p[2]/strong[3]").text
  puts "o número do pedido é: " + @nm_rev_comprada
  @nm_prod_comprado = driver.find_element(:xpath, "/html/body/div[3]/div[2]/div/p[2]/strong").text
  puts "O nome do produto comprado é: " + @nm_prod_comprado
  driver.find_element(:id, "user-name-link").click
  driver.find_elements(:css, ".user-profile a")[0].click
  #driver.find_element(:xpath, "/html/body/div[2]/div/div/div/ul/li/a").click
  @hist_nropedido = driver.find_elements(:css, ".order-info strong")[2].text
  @hist_nm_produto = driver.find_elements(:css, ".order-info strong")[0].text
  #@hist_nropedido = driver.find_element(:xpath, "/html/body/div[2]/div[2]/section/div/div[2]/ul/li[3]/ul/li/span[2]").text
  #@hist_nm_produto = driver.find_element(:xpath, "/html/body/div[2]/div[2]/section/div/div[2]/ul/li[3]/ul/li/div/a").text
  if @nm_rev_comprada == @hist_nropedido
  puts "Número do Pedido na Compra e no Histórico são iguais. Teste OK"
  else
  puts "Teste NOK"
  end
end
    
## Histórico primeira compra revistas grátis
def primeiracompragratis(driver)
  @rev_comprada = driver.find_element(:css, "#main-content div#order-success.order-confirmation p.order-info strong")[2].text
  puts "o número do pedido é: " + @rev_comprada
  @prod_comprado = driver.find_elements(:css, "#main-content div#order-success.order-confirmation p.order-info strong")[0].text
  puts "O nome do produto comprado é: " + @prod_comprado
  #driver.find_element(:css, "button.close").click
  #driver.find_element(:id, "user-name-link").click
  @driver.find_elements(:css, ".user-profile a")[1].click
  #driver.find_element(:xpath, "/html/body/div[2]/div/div/div/ul/li/a").click
  @hist_nropedido = driver.find_element(:xpath, "/html/body/div[2]/div[2]/section/div/div[2]/ul/li[3]/ul/li/span[2]").text
  @hist_nm_produto = driver.find_element(:xpath, "/html/body/div[2]/div[2]/section/div/div[2]/ul/li[3]/ul/li/div/a").text
  if @rev_comprada == @hist_nropedido
  puts "Número do Pedido na Compra e no Histórico são iguais. Teste OK"
  else
  puts "Teste NOK"
  end
end
## fim Revistas Grátis
end