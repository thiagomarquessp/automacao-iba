# encoding: utf-8
#!/usr/bin/env ruby
# https://github.com/stympy/faker
# Sample Test:Unit based test case using the selenium-client API
#
require "json"
require "selenium-webdriver"
require 'faker'
require 'clipboard'
require 'cpf_faker'
require "faker_credit_card"
gem "test-unit"
require "test/unit"
require "../classes/scripts"


### Pensar em melhores práticas sempre
### Alguns gits: https://github.com/ngauthier/domino e https://github.com/jnicklas/capybara
### https://github.com/watir/watir-webdriver/wiki/Page-Objects

class MassadeDados 

	attr_reader :nome, :enderecodata, :email, :empresa, :profissao, :telefonedata, :cep, :visa, :visapadrao, 
				:mc, :codseguranca, :cpf, :senha_padrao, :visa_ultimos_digitos


### Massa de Usuários. Será utilizada em todos os métodos.
### Será necessário pensar em algo paleativo para quando o fakegenerator e geradordecpf estiver fora do ar. 
### Pensar em algo como arquivo.csv como fonte de dados.
def fake_generator(driver)
  #Faker::Config.locale = 'pt-br'
  @nome = Faker::Name.name
  @email = Faker::Internet.email
  puts "O email utilizado para esse teste: " + @email
  @cpf = Faker::CPF.numeric
  @cep = "04679230"
  @visa = Faker::CreditCard::Visa.number(length: 16)
  @visa_ultimos_digitos = @visa[-4..-1]
  puts "O cartão utilizado para essa compra será: " + @visa
  @senha_padrao = "inicial1234"
  puts "Grave bem essa senha: " + @senha_padrao
end
### Fim do Método de Massa de Usuários ###
end

class Usuarios

def existente(driver)
 driver.get "https://store-qa.ibacloud.com.br/"
 driver.find_element(:css, "button.close").click
 driver.find_element(:id, "header-login-link").click
 driver.find_element(:id, "login_email").send_keys "a@b.com.br"
 driver.find_element(:id, "login_password").send_keys "inicial1234"
 driver.find_element(:id, "submit-signin").click
end
### Fim do Método de Usuário existente.
end

class CadastroGenerico

attr_reader :cupom

def setup
  @base_url = "https://store-qa.ibacloud.com.br/"
end

def teardown
  @driver.quit
  assert_equal [], @verification_errors
end

def  massausuario
  @massa ||= MassadeDados.new
end

def nome 
  massausuario.nome
end

def email 
  massausuario.email
end
# Método de Cadastro Completo - Fluxo Principal #
def cadastro_cliente(driver, base_url)
  puts "Fluxo principal de cadastro."
  massausuario.fake_generator(driver)
  driver.get (base_url)
  driver.find_element(:css, "button.close").click
  driver.find_element(:id, "header-sign-up-link").click
  driver.find_element(:id, "sign_up_email").send_keys massausuario.email
  driver.find_element(:id, "sign_up_email_confirmation").send_keys massausuario.email
  driver.find_element(:id, "sign_up_name").send_keys massausuario.nome
  driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
  driver.find_element(:id, "sign_up_password_confirmation").send_keys massausuario.senha_padrao
  driver.find_element(:id, "sign_up_terms_and_conditions").click
  driver.find_element(:css, "#sign-up-form .btn.info.btn-info").click
  sleep 10
  @cupom = driver.find_element(:css, ".coupon-code-container").text
  puts "Esse é cupom gerado pelo seu cadastro: " + @cupom
  driver.find_element(:css, "#coupon-modal .btn").click
end
### Fim do Método de Cadastro de Cliente ###
end