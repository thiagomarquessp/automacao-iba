# encoding: utf-8
#!/usr/bin/env ruby
#
# Sample Test:Unit based test case using the selenium-client API
#
require "json"
require "selenium-webdriver"
#require 'spreadsheet'
require "active_support/core_ext"
gem "test-unit"
require "test/unit"
require "../classes/scripts"


class CompraRevista <Test::Unit::TestCase
	def setup
		@driver = Selenium::WebDriver.for :firefox
    	@base_url = "http://store-qa.ibacloud.com.br/iba-clube"
    	@accept_next_alert = true
    	@driver.manage.timeouts.implicit_wait = 60
    	@verification_errors = []
	end

  	def teardown
    	@driver.quit
    	assert_equal [], @verification_errors
  	end
  
  	def element_present?(how, what)
    	@driver.find_element(how, what)
    true
 	rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  	end
  
  	def alert_present?()
    	@driver.switch_to.alert
    true
  	rescue Selenium::WebDriver::Error::NoAlertPresentError
    false
  	end
  
  	def verify(&blk)
    yield
  	rescue Test::Unit::AssertionFailedError => ex
    	@verification_errors << ex
  	end
  
  	def close_alert_and_get_its_text(how, what)
    	alert = @driver.switch_to().alert()
    	alert_text = alert.text
    if (@accept_next_alert) then
      	alert.accept()
    else
      	alert.dismiss()
    end
    	alert_text
  	ensure
    	@accept_next_alert = true
  	end

  	def  scripts_initialize
    	scripts = ScriptsJs.new
  	end

  	def  massausuario
      @massa ||= MassadeDados.new
  	end

  	def usuario
    usuario_existente = Usuarios.new
  	end


### Inicio dos Testes
	def test_iba_clube_cad_completo
  puts "Validação de Cadastro Completo no IbaClube"
	massausuario.fake_generator(@driver)
  @driver.get(@base_url)
	@driver.find_element(:xpath, "/html/body/section/div/a").click
	4.times { scripts_initialize.random_ibaclube_revistas(@driver) }
	@driver.find_element(:xpath, "/html/body/section/div/header/div/div[2]/a").click
	@driver.find_element(:id, "sign_up_email").send_keys massausuario.email
 	@driver.find_element(:id, "sign_up_email_confirmation").send_keys massausuario.email
 	@driver.find_element(:id, "sign_up_name").send_keys massausuario.nome
 	@driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
  @driver.find_element(:id, "sign_up_password_confirmation").send_keys massausuario.senha_padrao	
	@driver.find_element(:id, "sign_up_terms_and_conditions").click
	@driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[10]/button").click
	sleep 03
  #Aqui inicia o cadastro completo do cliente sem one click buy#
  @driver.find_element(:id, "user_address_postal_code").send_keys "04679230"
  sleep 05
  @driver.find_element(:id, "user_address_street_number").send_keys "12345"
  @driver.find_element(:id, "user_address_street_line2").send_keys "bloco XX apartamento XX"
  @driver.find_element(:id, "user_cpf").send_keys massausuario.cpf
  sleep 05
  @driver.find_element(:id, "terms-and-conditions").click
  @driver.find_element(:xpath, "/html/body/section/div/div[2]/div/div/form/button").click
	end

# Validação de Email com formato inválido
  def ic_validaEmailInvalido
  puts "Validação de email com formato inválido."
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)  
  @driver.find_element(:xpath, "/html/body/section/div/a").click
  4.times { scripts_initialize.random_ibaclube_revistas(@driver) }
  @driver.find_element(:xpath, "/html/body/section/div/header/div/div[2]/a").click
  @driver.find_element(:id, "sign_up_email").send_keys "abc123@"
  @driver.find_element(:id, "sign_up_email_confirmation").send_keys "123@"
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  @driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[10]/button").click
  @ic_verificaEmail = @driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[3]/div/label").text
  @ic_confirmaEmail = @driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[4]/div/label").text
  puts @ic_verificaEmail
  puts @ic_confirmaEmail
  end

# Validação de Email existente
  def ic_validaEmailExistente
  puts "Validação de cadastro com email já existente."
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)  
  @driver.find_element(:xpath, "/html/body/section/div/a").click
  4.times { scripts_initialize.random_ibaclube_revistas(@driver) }  
  @driver.find_element(:xpath, "/html/body/section/div/header/div/div[2]/a").click
  @driver.find_element(:id, "sign_up_email").send_keys "icuserexist@iba.com"
  @driver.find_element(:id, "sign_up_email_confirmation").send_keys "icuserexist@iba.com"
  @driver.find_element(:id, "sign_up_name").send_keys massausuario.nome
  @driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
  @driver.find_element(:id, "sign_up_password_confirmation").send_keys massausuario.senha_padrao  
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  @driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[10]/button").click
  @ic_duplicidadeEmail = @driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[2]/ul/li").text
  puts @ic_duplicidadeEmail
  verify {assert(@driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[2]/ul/li").text.include?("Este e-mail já está cadastrado.")==true)}
  sleep 10
  end

  def ic_validaEmailDiferentes
  puts "Testes de validação de cadastro com emails diferentes"
  massausuario.fake_generator(@driver)
  @driver.get(@base_url)
  @driver.find_element(:xpath, "/html/body/section/div/a").click
  4.times { scripts_initialize.random_ibaclube_revistas(@driver) }  
  @driver.find_element(:xpath, "/html/body/section/div/header/div/div[2]/a").click
  @driver.find_element(:id, "sign_up_email").send_keys "icuserexist@iba.com"
  @driver.find_element(:id, "sign_up_email_confirmation").send_keys "icuserexist@iba.com.br"
  @driver.find_element(:id, "sign_up_name").send_keys massausuario.nome
  @driver.find_element(:id, "sign_up_password").send_keys massausuario.senha_padrao
  @driver.find_element(:id, "sign_up_password_confirmation").send_keys massausuario.senha_padrao  
  @driver.find_element(:id, "sign_up_terms_and_conditions").click
  @driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[10]/button").click
  @ic_EmailDiferente = @driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[4]/div/label").text
  puts @ic_EmailDiferente
  verify {assert(@driver.find_element(:xpath, "/html/body/section/div/div[2]/div/form/div[4]/div/label").text.include?("Os e-mails não coincidem. Por favor, digite novamente.")==true)}
    if @ic_EmailDiferente == "Os e-mails não coincidem. Por favor, digite novamente."
      puts "Esse teste deu tudo certo =)"
    else
      puts "=( que pena!!! A mensagem está errada"
    end
  end



end
